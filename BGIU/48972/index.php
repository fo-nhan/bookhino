<?php
session_start();

require './config.php';
require './request.php';

if (isset($_SESSION['token']) && isset($_SESSION['user_id']) && $_SESSION['token'] && $_SESSION['user_id']) {
    $sql = "SELECT * FROM access WHERE `access`.`id` = ? AND `access`.`token` = ? AND `access`.`status` = 'normal' ";
    $request = REQUEST_GET($sql, "ss", [$_SESSION['user_id'], $_SESSION['token']]);
    if ($request['status'] !== 200) {
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s', strtotime('+5 minutes', strtotime($startTime)));
        $sql4 = "UPDATE `access` SET `timeEnd` = ? WHERE `access`.`id` = ? AND `access`.`token` = ? AND `access`.`status` = 'normal'";
        $request4 = REQUEST_POST($sql4, "sss", [$cenvertedTime, $_SESSION['user_id'], $_SESSION['token']]);
        session_unset();
        session_destroy();
        echo '<script> alert("You are logged in somewhere else!"); </script> ';
        echo '<script> window.location="/login.php"; </script> ';
    }
} else {
    session_unset();
    session_destroy();
    echo '<script> window.location="/login.php"; </script> ';
}
if (!isset($_SESSION['book1'])) {

    echo '<script> window.location="/login.php"; </script> ';
} else if ($_SESSION['book1'] == 0) {
    echo '<script> window.location="/index.php"; </script> ';
}

?>

<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="apple-mobile-web-app-capable" content="yes"><meta name="apple-mobile-web-app-status-bar-style" content="black"><meta name="viewport" content="target-densitydpi=device-dpi, width=device-width"><!--<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">--><meta name="author" content="Netex"><meta name="dcterms.created" content="05-02-2016 08:35:57"><!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=Edge"><![endif]--><link data-move-head="true" rel="stylesheet" href="commoncontents/kidsBox/js/libs/mediaelement/mediaelementplayer.min.css"><link data-move-head="true" rel="stylesheet" href="commoncontents/kidsBox/css/kidsBox_min.css"><link data-move-head="true" rel="stylesheet" href="commoncontents/kidsBox/js/netex/htmlActivity/css/testLC_kidsBox_min.css"><link data-move-head="true" rel="stylesheet" href="commoncontents/kidsBox/css/translate.css"><link data-move-head="true" rel="stylesheet" href="commoncontents/css/simon.min.css"><title>Presentation plus</title><link rel="stylesheet" href="commoncontents/ntxtmp/css/libs/jquery-ui.min.css"><link rel="stylesheet" href="commoncontents/ntxtmp/css/libs/normalize.css"><link rel="stylesheet" href="commoncontents/ntxtmp/css/libs/animate.min.css"><link rel="stylesheet" href="commoncontents/ntxtmp/js/netex/plugins/video/netexVideoSkin.css"><link rel="stylesheet" href="commoncontents/ntxtmp/js/netex/plugins/audio/netexAudioSkin.css"><link rel="stylesheet" href="commoncontents/ntxtmp/css/netex/ntxtmp_min.css"><meta name="learningmaker-version" content="4.2.3.10"></head><body data-course-id="CODE-48972" class="teacher-package">

            
<script>
    window.addEventListener("click", () => {
      const token = localStorage.getItem('token');
      if (token) {
        fetch('checkpoint.php', {
            method: 'GET', // or 'PUT'
            headers: {
              'Content-Type': 'application/json',
            },
          })
          .then((response) => response.json())
          .then((data) => {
            if(data && data.checkpoint){
              alert("You are logged in somewhere else!");
              <?php echo 'window.location="/login.php"; '; ?>
            }
          })
      }
    })
  </script>

<div id="page-wrap" class="landscape">
    <noscript id="gaTrackingConfig" data-ga-tracking-id="UA-8600104-1" data-ga-tracking-product-name="presentationplus" data-ga-tracking-course-name="supersafariAE" data-ga-tracking-level-name="level1" data-ga-tracking-unit-name=""></noscript>

<div id="main-menu-wrap">
    <ul class="main-menu enabled" id="main-menu"><li class="main-menu-item home active">  
            <a href="../index.php" title="Home" class="sprite"></a>
        </li>
        <li class="main-menu-item pupil-book active">
            <span class="sprite" title="Student's Book"></span>
            <ul class="navigator pupil-book-navigator"></ul></li>
        <li class="main-menu-item activity-book">
           <span class="sprite" title="Workbook"></span>
            <ul class="navigator activity-book-navigator"></ul></li>
        <li class="main-menu-item media">
            <span class="sprite" title="Media Library"></span>
            <ul class="sub-menu" id="sub-menu-media"><li class="sub-menu-item video-popup">    
                    <span class="sprite" title="Videos"></span>
                </li>
                <li class="sub-menu-item audio-popup">   
                    <span class="sprite" title="Extra Audio"></span>
                </li>
                <li class="sub-menu-item flashcard-popup">    
                    <span class="sprite" title="Flashcards"></span>
                </li>
            </ul></li>
        <li class="main-menu-item resource-popup">
            <span class="sprite" title="Resources"></span>
        </li>
        <li class="main-menu-item link-popup active">
            <span class="sprite" title="Links"></span>
        </li>
        <li class="main-menu-item bookmark active">
            <span class="sprite" title="Bookmarks"></span>
            <ul class="sub-menu" id="sub-menu-bookmark"><li class="sub-menu-item return active">   
                    <span class="sprite" title="Return"></span>
                </li>
                <li class="sub-menu-item add active">  
                    <span class="sprite" title="Add bookmark"></span>
                </li>
                <li class="sub-menu-item list-popup active">   
                    <span class="sprite" title="Bookmark list"></span>
                </li>
            </ul></li>
        <li class="main-menu-item note active">
            <span class="sprite" title="Notes"></span>
            <ul class="sub-menu" id="sub-menu-note"><li class="sub-menu-item add-note active">      
                    <span class="sprite" title="Add Note"></span>
                </li>
                <li class="sub-menu-item add-attachment active">    
                    <span class="sprite" title="Add Attachment"></span>
                </li>
            </ul></li>
        <li class="main-menu-item mask active">
            <span class="sprite" title="Mask"></span>
        </li>
        <li class="main-menu-item notepad active">
            <span class="sprite" title="Notepad"></span>
        </li>
    </ul><div id="title-bar">
        Unit 1 - My class                    <span class="sprite answer-key-toggle" title="Answer Key"></span>
                <span class="sprite zoom zoom-in" title="Zoom In"></span>
        <div id="answer-key-nav">
            <ul></ul></div>
    </div>
    
</div>
    

 
    <noscript data-type="popup" data-id="video-popup"> 

        <div class="popup video-popup">
            <span class="sprite close-popup"></span>
            <h2 class="popup-title">Video</h2>

            <div id="video-player-wrap">
                <div id="video-player-placeholder"></div>
                <div id="transcript-buttons" class="video hidden">
                    <button id="toggle-transcript">Show transcript</button>
                    <button id="transcript-highlight" class="sprite hidden"></button>
                </div>
            </div>
            <div id="video-gallery-wrap">
                <ul id="video-gallery"><li>
                                    <img class="video-thumb" data-id="video_1106868ce6a551e3ba261b9ac434ceaf" src="resources/videos/u1_song.JvcfNRk5"></li>
                                                                                   
                                <li>
                                    <img class="video-thumb" data-id="video_00d0d056542ea824a7c0dd69cdd7c56c" src="resources/videos/u1_karaoke.JvcfNRk5"></li>
                                                                                   
                                <li>
                                    <img class="video-thumb" data-id="video_98a9e76f8e31e0c6e45a8baa2aa994a8" src="resources/videos/u1_story.JvcfNRk5"></li>
                                                                                        </ul></div>
            <div id="transcript-wrap" class="hidden"></div>
        </div>
    </noscript> 

           

    <noscript data-type="video" id="video_1106868ce6a551e3ba261b9ac434ceaf">
        
        <div class="media">
            <div class="media-body">
                                <div class="netex-video-player" data-video-swf-url="./netexVideo.swf" data-video-player-id="video_1106868ce6a551e3ba261b9ac434ceaf" data-height="576" data-width="720">
                    <div class="netex-video-responsive" style="padding-bottom:80%;">
                        <div class="netex-video-container">
                                                                        <div class="mejs-overlay mejs-layer mejs-overlay-play" style="width: 100%; height: 100%;">
                            <div class="mejs-overlay-button" style="margin-top: -35px;"></div>                                        
                        </div>
                        <video preload="none" webkit-playsinline="1"><source src="resources/videos/u1_song.MvTDKVFm" type="video/webm"></source></video><img src="resources/videos/u1_song.JvcfNRk5" class="netex-video-poster"></div>
                    </div>
                    <div class="netex-video-controls-container">
                        <div class="netex-video-progress-bar-container">
                            <div class="netex-video-progress-bar">
                                <div class="netex-video-progress-bar-inner"></div>
                            </div>
                        </div>
                        <div class="netex-video-controls">
                            <div class="netex-video-controls-item netex-video-button-play-pause-restart netex-video-button-play"></div>
                            <div class="netex-video-volume-control"><div class="netex-video-controls-item netex-video-volume-button netex-video-volume-large"></div><div class="netex-video-volume-panel"><div class="netex-video-volume-slider netex-video-volume-draggable"><div class="netex-video-volume-slider-foreground"></div></div></div></div>
                            <div class="netex-video-time-container"><span class="netex-video-time-current">00:00</span><span class="netex-video-time-separator"> / </span><span class="netex-video-time-duration">00:00</span></div>
                            <div class="netex-video-tools-item netex-video-button-fullscreen netex-video-button-fullscreen-enter" title="Fullscreen On"></div>
                                                    </div>
                    </div>
                </div>
            </div>
        </div>
            </noscript>
           

    <noscript data-type="video" id="video_00d0d056542ea824a7c0dd69cdd7c56c">
        
        <div class="media">
            <div class="media-body">
                                <div class="netex-video-player" data-video-swf-url="./netexVideo.swf" data-video-player-id="video_00d0d056542ea824a7c0dd69cdd7c56c" data-height="576" data-width="720">
                    <div class="netex-video-responsive" style="padding-bottom:80%;">
                        <div class="netex-video-container">
                                                                        <div class="mejs-overlay mejs-layer mejs-overlay-play" style="width: 100%; height: 100%;">
                            <div class="mejs-overlay-button" style="margin-top: -35px;"></div>                                        
                        </div>
                        <video preload="none" webkit-playsinline="1"><source src="resources/videos/u1_karaoke.MvTDKVFm" type="video/webm"></source></video><img src="resources/videos/u1_karaoke.JvcfNRk5" class="netex-video-poster"></div>
                    </div>
                    <div class="netex-video-controls-container">
                        <div class="netex-video-progress-bar-container">
                            <div class="netex-video-progress-bar">
                                <div class="netex-video-progress-bar-inner"></div>
                            </div>
                        </div>
                        <div class="netex-video-controls">
                            <div class="netex-video-controls-item netex-video-button-play-pause-restart netex-video-button-play"></div>
                            <div class="netex-video-volume-control"><div class="netex-video-controls-item netex-video-volume-button netex-video-volume-large"></div><div class="netex-video-volume-panel"><div class="netex-video-volume-slider netex-video-volume-draggable"><div class="netex-video-volume-slider-foreground"></div></div></div></div>
                            <div class="netex-video-time-container"><span class="netex-video-time-current">00:00</span><span class="netex-video-time-separator"> / </span><span class="netex-video-time-duration">00:00</span></div>
                            <div class="netex-video-tools-item netex-video-button-fullscreen netex-video-button-fullscreen-enter" title="Fullscreen On"></div>
                                                    </div>
                    </div>
                </div>
            </div>
        </div>
            </noscript>
           

    <noscript data-type="video" id="video_98a9e76f8e31e0c6e45a8baa2aa994a8">
        
        <div class="media">
            <div class="media-body">
                                <div class="netex-video-player" data-video-swf-url="./netexVideo.swf" data-video-player-id="video_98a9e76f8e31e0c6e45a8baa2aa994a8" data-height="576" data-width="785">
                    <div class="netex-video-responsive" style="padding-bottom:73.375796178344%;">
                        <div class="netex-video-container">
                                                                        <div class="mejs-overlay mejs-layer mejs-overlay-play" style="width: 100%; height: 100%;">
                            <div class="mejs-overlay-button" style="margin-top: -35px;"></div>                                        
                        </div>
                        <video preload="none" webkit-playsinline="1"><source src="resources/videos/u1_story.MvTDKVFm" type="video/webm"></source></video><img src="resources/videos/u1_story.JvcfNRk5" class="netex-video-poster"></div>
                    </div>
                    <div class="netex-video-controls-container">
                        <div class="netex-video-progress-bar-container">
                            <div class="netex-video-progress-bar">
                                <div class="netex-video-progress-bar-inner"></div>
                            </div>
                        </div>
                        <div class="netex-video-controls">
                            <div class="netex-video-controls-item netex-video-button-play-pause-restart netex-video-button-play"></div>
                            <div class="netex-video-volume-control"><div class="netex-video-controls-item netex-video-volume-button netex-video-volume-large"></div><div class="netex-video-volume-panel"><div class="netex-video-volume-slider netex-video-volume-draggable"><div class="netex-video-volume-slider-foreground"></div></div></div></div>
                            <div class="netex-video-time-container"><span class="netex-video-time-current">00:00</span><span class="netex-video-time-separator"> / </span><span class="netex-video-time-duration">00:00</span></div>
                            <div class="netex-video-tools-item netex-video-button-fullscreen netex-video-button-fullscreen-enter" title="Fullscreen On"></div>
                                                    </div>
                    </div>
                </div>
            </div>
        </div>
            </noscript>
    
 
    <noscript data-type="popup" data-id="audio-popup"> 
        <div class="popup audio-popup">
                <span class="sprite close-popup"></span>
            <h2 class="popup-title">Audio</h2>
                <ul id="audio-gallery"><div class="audio-gallery-row">
                                                                           
                                <li>
                                    <span class="audio-title" data-id="audio_e13f381da41581a31ae1d556c3218ab6">
                                         <span class="sprite audio-bullet"></span>
                                        Unit 1 chant                                    </span>
                                </li>
                                                                                                               
                                <li>
                                    <span class="audio-title" data-id="audio_ff86790e57210a333f19b576230ef5a9">
                                         <span class="sprite audio-bullet"></span>
                                        Unit 1 song                                    </span>
                                </li>
                                                                                                               
                                <li>
                                    <span class="audio-title" data-id="audio_ebf8d9d12e8ccd1c728755e778c1cd1d">
                                         <span class="sprite audio-bullet"></span>
                                        Unit 1 story                                    </span>
                                </li>
                                                                                                                        </div>
                </ul><div id="audio-player-wrap"></div>
                <div id="transcript-buttons" class="audio hidden">
                    <button id="toggle-transcript">Show transcript</button>
                    <button id="transcript-highlight" class="sprite hidden"></button>
                </div>
                <div id="transcript-wrap" class="hidden"></div>
        </div>
    </noscript> 
    

    
                       
                <noscript data-type="audio" id="audio_e13f381da41581a31ae1d556c3218ab6">
                    <div class="media presentationPlus">
                        <div class="media-body">
                                                        <div class="netex-audio-player" data-audio-swf-url="./netexAudio.swf" data-audio-player-id="audio_e13f381da41581a31ae1d556c3218ab6">
                                <div class="netex-audio-container">
                                    <audio preload="none"><source src="resources/pop_audio/1a.a2fWF5NXu" type="audio/ogg"></source></audio></div>
                                <div class="netex-audio-controls-container">
                                    <div class="netex-audio-controls">
                                        <div class="netex-audio-controls-item netex-audio-button-play-pause-restart netex-audio-button-play"></div>
                                        <div class="netex-audio-time-container">
                                            <span class="netex-audio-time-current">00:00</span>
                                            <div class="netex-audio-progress-bar-container">
                                                <div class="netex-audio-progress-bar">
                                                    <div class="netex-audio-progress-bar-inner"></div>
                                                </div>
                                            </div>
                                            <span class="netex-audio-time-duration">00:00</span>
                                        </div>
                                        <div class="netex-audio-volume-control">
                                                <div class="netex-audio-volume-slider netex-audio-volume-draggable">
                                                    <div class="netex-audio-volume-slider-foreground"></div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="transcript hidden  " id="transcript_889be10989f040d767fd6b4243319592">

        
    	 <p>Look! It's my chair.<br></p><p>Look! It's my chair.</p><p>Look! It's my chair.</p><p>Look, look, look!</p><p><br></p><p>Look! It's my pencil.</p><p>Look! It's my pencil.</p><p>Look! It's my pencil.</p><p>Look, look, look!</p><p><br></p><p>Look! It's my bag.</p><p>Look! It's my bag.</p><p>Look! It's my bag.</p><p>Look, look, look!</p><p><br></p><p>Look! It's my book.</p><p>Look! It's my book.</p><p>Look! It's my book.</p><p>Look, look, look!<br></p></div>

                </noscript>
                        

    
                       
                <noscript data-type="audio" id="audio_ff86790e57210a333f19b576230ef5a9">
                    <div class="media presentationPlus">
                        <div class="media-body">
                                                        <div class="netex-audio-player" data-audio-swf-url="./netexAudio.swf" data-audio-player-id="audio_ff86790e57210a333f19b576230ef5a9">
                                <div class="netex-audio-container">
                                    <audio preload="none"><source src="resources/pop_audio/1b.a2fWF5NXu" type="audio/ogg"></source></audio></div>
                                <div class="netex-audio-controls-container">
                                    <div class="netex-audio-controls">
                                        <div class="netex-audio-controls-item netex-audio-button-play-pause-restart netex-audio-button-play"></div>
                                        <div class="netex-audio-time-container">
                                            <span class="netex-audio-time-current">00:00</span>
                                            <div class="netex-audio-progress-bar-container">
                                                <div class="netex-audio-progress-bar">
                                                    <div class="netex-audio-progress-bar-inner"></div>
                                                </div>
                                            </div>
                                            <span class="netex-audio-time-duration">00:00</span>
                                        </div>
                                        <div class="netex-audio-volume-control">
                                                <div class="netex-audio-volume-slider netex-audio-volume-draggable">
                                                    <div class="netex-audio-volume-slider-foreground"></div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="transcript hidden  " id="transcript_112e8d145ad26660e60029bd542ac978">

        
    	 <p>Clean up, clean up,<br></p><p>Clean up your book.</p><p>Clean up, clean up,</p><p>Clean up your book.</p><p><br></p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!</p><p><br></p><p>Clean up, clean up,</p><p>Clean up your chair.</p><p>Clean up, clean up,</p><p>Clean up your chair.</p><p><br></p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!</p><p><br></p><p>Clean up, clean up,</p><p>Clean up your pencil.</p><p>Clean up, clean up,</p><p>Clean up your pencil.</p><p><br></p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!</p><p><br></p><p>Clean up, clean up,</p><p>Clean up your bag.</p><p>Clean up, clean up,</p><p>Clean up your bag.</p><p><br></p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!</p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!<br></p></div>

                </noscript>
                        

    
                       
                <noscript data-type="audio" id="audio_ebf8d9d12e8ccd1c728755e778c1cd1d">
                    <div class="media presentationPlus">
                        <div class="media-body">
                                                        <div class="netex-audio-player" data-audio-swf-url="./netexAudio.swf" data-audio-player-id="audio_ebf8d9d12e8ccd1c728755e778c1cd1d">
                                <div class="netex-audio-container">
                                    <audio preload="none"><source src="resources/pop_audio/1c.a2fWF5NXu" type="audio/ogg"></source></audio></div>
                                <div class="netex-audio-controls-container">
                                    <div class="netex-audio-controls">
                                        <div class="netex-audio-controls-item netex-audio-button-play-pause-restart netex-audio-button-play"></div>
                                        <div class="netex-audio-time-container">
                                            <span class="netex-audio-time-current">00:00</span>
                                            <div class="netex-audio-progress-bar-container">
                                                <div class="netex-audio-progress-bar">
                                                    <div class="netex-audio-progress-bar-inner"></div>
                                                </div>
                                            </div>
                                            <span class="netex-audio-time-duration">00:00</span>
                                        </div>
                                        <div class="netex-audio-volume-control">
                                                <div class="netex-audio-volume-slider netex-audio-volume-draggable">
                                                    <div class="netex-audio-volume-slider-foreground"></div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="transcript hidden  " id="transcript_dbefd7d4e4f01c2fc1d513c01f1c2cee">

        
    	 <p>1.<br></p><p>GINA: Hello, Polly.</p><p>POLLY: Hello, Gina.</p><p>2.</p><p>MIKE: Hello, Leo.</p><p>LEO: Hello, Mike.</p><p>3.</p><p>TEACHER: Hello.</p><p>ANIMALS: Hello.</p><p>TEACHER: Sit down, please.</p><p>4.</p><p>MIKE: It's my chair.</p><p>LEO: No, it's my chair.</p><p>TEACHER: Mike! Leo! Stop it.</p><p>MIKE, LEO: Ooooph!</p><p>5.</p><p>LEO: Sorry.</p><p>MIKE: Ooh. Sorry.<br></p></div>

                </noscript>
                          
    <noscript data-type="popup" data-id="flashcard-popup"> 
<div class="popup flashcard-popup">
        <span class="sprite close-popup"></span>
    <h2 class="popup-title">Flashcards</h2>
    
    <div class="carousel">
        <span class="sprite nav nav-prev enabled" id="prev-flashcard"></span>
        <div class="carousel-content-wrap">
            <ul class="carousel-content"><li class="flashcard-thumb">
		<img src="resources/flashcards/ss_ae_l1_fc_p10_th.JvcfNRk5" data-type="thumb" data-id="854fe57d41a8cbeess_ae_l1_fc_p10" data-title="bag" data-rel-fullscreen="resources/flashcards/ss_ae_l1_fc_p10.JvcfNRk5"></li>

			<li class="flashcard-thumb">
		<img src="resources/flashcards/ss_ae_l1_fc_p12_th.JvcfNRk5" data-type="thumb" data-id="854fe57d41a8cbeess_ae_l1_fc_p12" data-title="pencil" data-rel-fullscreen="resources/flashcards/ss_ae_l1_fc_p12.JvcfNRk5"></li>

			<li class="flashcard-thumb">
		<img src="resources/flashcards/ss_ae_l1_fc_p14_th.JvcfNRk5" data-type="thumb" data-id="854fe57d41a8cbeess_ae_l1_fc_p14" data-title="book" data-rel-fullscreen="resources/flashcards/ss_ae_l1_fc_p14.JvcfNRk5"></li>

			<li class="flashcard-thumb">
		<img src="resources/flashcards/ss_ae_l1_fc_p16_th.JvcfNRk5" data-type="thumb" data-id="854fe57d41a8cbeess_ae_l1_fc_p16" data-title="chair" data-rel-fullscreen="resources/flashcards/ss_ae_l1_fc_p16.JvcfNRk5"></li>

            </ul></div>
        <span class="sprite nav nav-next enabled" id="next-flashcard"></span>
    </div>
</div>
 
    </noscript> 
  
    
    <noscript data-type="popup" data-id="link-popup"> 
<div class="popup link-popup">
        <span class="sprite close-popup"></span>
    <h2 class="popup-title">Links</h2>
    <ul id="list-link"><li class="link">
                <a target="_blank" href="http://www.cambridgelms.org/primary/p/splash">
            Cambridge LMS (online homework)        </a>
    </li>

    <li class="link">
                <a target="_blank" href="http://www.cambridge.org/gb/cambridgeenglish/catalog/primary/super-safari-american-english">
            Super Safari website        </a>
    </li>

    <li class="link">
                <a target="_blank" href="mailto:lms.support@cambridge.org">
            Customer support        </a>
    </li>
    </ul><div class="buttons-wrap">
        <button id="add-link-button" class="button">Add link</button>
    </div>
</div>
 
    </noscript> 

    <noscript data-type="dialog" id="new-link-dialog-template"> 
        <div id="new-link-dialog">
            Link name <br><input type="text" id="link-name" value=""><br>
            Link address <br><input type="text" value="" id="link-address" placeholder="www."><br><div class="buttons-wrap">
                <button id="new-link-button-accept" class="button">Accept</button>
                <button id="new-link-button-cancel" class="button">Cancel</button>
            </div>
        </div>
    </noscript>
        

     
    <noscript data-type="popup" data-id="resource-popup"> 
<div class="popup resource-popup">
        <span class="sprite close-popup"></span>
    <h2 class="popup-title">Resources</h2>
    <ul><li class="resource">
                <a target="_blank" href="resources/resources/pp_help_guide.pdf">
            Help guide 
        </a>
    </li>

 
    
    <li class="resource">
                <a target="_blank" href="resources/resources/tb_u1.pdf">
            Teacher’s Book Unit 1 
        </a>
    </li>
    </ul></div>
 
    </noscript> 

<noscript data-type="popup" data-id="pupil-popup"> 
    <div class="popup pupil-popup ">
        <h2 class="popup-title">Student's Book</h2>
        <div class="carousel">
            <span class="sprite  nav nav-prev enabled" id="prev-book-page"></span>
            <div class="carousel-content-wrap">
                <ul class="carousel-content"><li class="page-thumb" data-book="pupil" data-page-id="cb868818f898822css_ae_l1_pb_p08">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_pb_p08_th.JvcfNRk5"><div class="page-number">
                                Page 08                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="pupil" data-page-id="cb869919f999922css_ae_l1_pb_p09">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_pb_p09_th.JvcfNRk5"><div class="page-number">
                                Page 09                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="pupil" data-page-id="cb868818f898822css_ae_pb_p10">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_pb_p10_th.JvcfNRk5"><div class="page-number">
                                Page 10                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="pupil" data-page-id="cb868818f898822css_ae_pb_p11">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_pb_p11_th.JvcfNRk5"><div class="page-number">
                                Page 11                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="pupil" data-page-id="cb868818f898822css_ae_pb_p12">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_pb_p12_th.JvcfNRk5"><div class="page-number">
                                Page 12                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="pupil" data-page-id="cb868818f898822css_ae_pb_p13">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_pb_p13_th.JvcfNRk5"><div class="page-number">
                                Page 13                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="pupil" data-page-id="cb868818f898822css_ae_pb_p14">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_pb_p14_th.JvcfNRk5"><div class="page-number">
                                Page 14                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="pupil" data-page-id="cb868818f898822css_ae_pb_p15">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_pb_p15_th.JvcfNRk5"><div class="page-number">
                                Page 15                            </div>
                        </li>
                                    </ul></div>
            <span class="sprite  nav nav-next enabled" id="next-book-page"></span>
        </div>
    </div>
</noscript>
<noscript data-type="popup" data-id="activity-popup"> 
    <div class="popup activity-popup">
        <h2 class="popup-title">Workbook</h2>
        <div class="carousel">
            <span class="sprite  nav nav-prev enabled" id="prev-book-page"></span>
            <div class="carousel-content-wrap">
                <ul class="carousel-content"><li class="page-thumb" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p08">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_ab_p08_th.JvcfNRk5"><div class="page-number">
                                Page 08                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="activity" data-page-id="cb869919f999922css_ae_l1_ab_p09">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_ab_p09_th.JvcfNRk5"><div class="page-number">
                                Page 09                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p10">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_ab_p10_th.JvcfNRk5"><div class="page-number">
                                Page 10                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p11">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_ab_p11_th.JvcfNRk5"><div class="page-number">
                                Page 11                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p12">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_ab_p12_th.JvcfNRk5"><div class="page-number">
                                Page 12                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p13">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_ab_p13_th.JvcfNRk5"><div class="page-number">
                                Page 13                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p14">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_ab_p14_th.JvcfNRk5"><div class="page-number">
                                Page 14                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p15">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_ab_p15_th.JvcfNRk5"><div class="page-number">
                                Page 15                            </div>
                        </li>
                                    </ul></div>
            <span class="sprite  nav nav-next enabled" id="next-book-page"></span>
        </div>
    </div>
</noscript>
<noscript data-type="popup" data-id="bookmark-list-popup"> 
    <div class="popup bookmark-list-popup">
        <span class="sprite close-popup"></span>
        <h2 class="popup-title">Bookmarks</h2>
        <div id="bookmark-list">

        </div>
    </div>
</noscript>


    <div id="popupView" class="ntx-popup"></div>
    <div id="pageView" class="content" touch-action="none" data-trans-out-forward="fadeOut" data-trans-out-rewind="fadeOut" data-trans-in="fadeIn"></div>
    
 <noscript data-page-title="pb_08" data-page-number="08" data-type="content" data-book="pupil" data-page-id="cb868818f898822css_ae_l1_pb_p08">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 130px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="ea39eca166d232cc4beecf18555d5420" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_08.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_75c2b8ca723374272f3cf858f67a55a1">

        
    	 <p>LEO: Bag!<br></p><p>POLLY: Pencil … Pencil!</p><p>GINA: Book!</p><p>MIKE: Chair!</p><p><br></p><p>bag, pencil, book, chair</p></div>

            </div>
            
<div class="sprite activity-resource" rel-activity="activity_013d3a3e63c4ce3d6e22c073c30506f6" title="Activity">
	</div>
<div class="sprite activity-resource" rel-activity="activity_4d3f252782cdc28cc168e7fb1e7d9a2b" title="Activity">
	</div></div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_pb_p08.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_l1_pb_p08" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="pb_09" data-page-number="09" data-type="content" data-book="pupil" data-page-id="cb869919f999922css_ae_l1_pb_p09">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 45px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="a729871dbd75d68c0bd3a82194f3a0da" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_09.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_36afe65bdfaadf85edb7400dda6d5c48">

        
    	 <p>Look! It's my chair.<br></p><p>Look! It's my chair.</p><p>Look! It's my chair.</p><p>Look, look, look!</p><p><br></p><p>Look! It's my pencil.</p><p>Look! It's my pencil.</p><p>Look! It's my pencil.</p><p>Look, look, look!</p><p><br></p><p>Look! It's my bag.</p><p>Look! It's my bag.</p><p>Look! It's my bag.</p><p>Look, look, look!</p><p><br></p><p>Look! It's my book.</p><p>Look! It's my book.</p><p>Look! It's my book.</p><p>Look, look, look!<br></p></div>

            </div>
            </div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_pb_p09.JvcfNRk5"><canvas id="page-canvas-cb869919f999922css_ae_l1_pb_p09" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="pb_10" data-page-number="10" data-type="content" data-book="pupil" data-page-id="cb868818f898822css_ae_pb_p10">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 60px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="f2ba5bc8b2d5b2a3aec66f06b12fb151" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_11.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_3e112d1e2e6db58cf6520018b4e594e7">

        
    	 <p>Hands up<br></p><p>Hands down</p><p>Clap your hands</p><p>Hands up</p><p>Clap your hands</p><p>Hands down</p><p>Clap your hands</p><p>Hands up</p><p>Hands down</p><p>Clap your hands<br></p></div>

            </div>
            </div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_pb_p10.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_pb_p10" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="pb_11" data-page-number="11" data-type="content" data-book="pupil" data-page-id="cb868818f898822css_ae_pb_p11">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 60px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="dfca1f3b0f70ef10bcec25f81e7b3d10" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_13.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_416a4b63db28db902ab5a1e7209260b9">

        
    	 <p>Clean up, clean up,<br></p><p>Clean up your book.</p><p>Clean up, clean up,</p><p>Clean up your book.</p><p><br></p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!</p><p><br></p><p>Clean up, clean up,</p><p>Clean up your chair.</p><p>Clean up, clean up,</p><p>Clean up your chair.</p><p><br></p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!</p><p><br></p><p>Clean up, clean up,</p><p>Clean up your pencil.</p><p>Clean up, clean up,</p><p>Clean up your pencil.</p><p><br></p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!</p><p><br></p><p>Clean up, clean up,</p><p>Clean up your bag.</p><p>Clean up, clean up,</p><p>Clean up your bag.</p><p><br></p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!</p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!<br></p></div>

            </div>
            

    
                        <div id="8657a7364d95c3386fb2c02217288152" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_14.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_c40c7a2052b938bf781bb17e9c3bacf8">

        
    	 <p>Clean up, clean up,<br></p><p>Clean up your book.</p><p>Clean up, clean up,</p><p>Clean up your book.</p><p><br></p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!</p><p><br></p><p>Clean up, clean up,</p><p>Clean up your chair.</p><p>Clean up, clean up,</p><p>Clean up your chair.</p><p><br></p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!</p><p><br></p><p>Clean up, clean up,</p><p>Clean up your pencil.</p><p>Clean up, clean up,</p><p>Clean up your pencil.</p><p><br></p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!</p><p><br></p><p>Clean up, clean up,</p><p>Clean up your bag.</p><p>Clean up, clean up,</p><p>Clean up your bag.</p><p><br></p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!</p><p>Wo-ho ho-ho-ho! Clean clean!</p><p>Wo-ho ho-ho-ho! Clean up!<br></p></div>

            </div>
            </div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_pb_p11.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_pb_p11" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="pb_12" data-page-number="12" data-type="content" data-book="pupil" data-page-id="cb868818f898822css_ae_pb_p12">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 60px;">
	
            <div class="sprite icon-toggle"></div>
            
<div class="sprite activity-resource" rel-activity="activity_63c9736cd4bf28b1f320e5a85a3f4ef1" title="Activity">
	</div>

    
                        <div id="1e98b8362f6562a08102717f83fd2480" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_15.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_b49de0bc8343375d14d150f1d8983bd6">

        
    	 <p>1.<br></p><p>GINA: Hello, Polly.</p><p>POLLY: Hello, Gina.</p><p>2.</p><p>MIKE: Hello, Leo.</p><p>LEO: Hello, Mike.</p><p>3.</p><p>TEACHER: Hello.</p><p>ANIMALS: Hello.</p><p>TEACHER: Sit down, please.</p><p>4.</p><p>MIKE: It's my chair.</p><p>LEO: No, it's my chair.</p><p>TEACHER: Mike! Leo! Stop it.</p><p>MIKE, LEO: Ooooph!</p><p>5.</p><p>LEO: Sorry.</p><p>MIKE: Ooh. Sorry.<br></p></div>

            </div>
            </div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_pb_p12.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_pb_p12" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="pb_13" data-page-number="13" data-type="content" data-book="pupil" data-page-id="cb868818f898822css_ae_pb_p13">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
                
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_pb_p13.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_pb_p13" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="pb_14" data-page-number="14" data-type="content" data-book="pupil" data-page-id="cb868818f898822css_ae_pb_p14">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 120px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="5a4f9c67f5a1538095719a631a18f684" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_17.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_2dc88125aee59f91eb98250075cbf2a6">

        
    	 <p>look<br></p><p>listen</p><p>join in</p><p>listen</p><p>join in</p><p>look</p><p>listen</p><p>look</p><p>join in<br></p></div>

            </div>
            </div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_pb_p14.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_pb_p14" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="pb_15" data-page-number="15" data-type="content" data-book="pupil" data-page-id="cb868818f898822css_ae_pb_p15">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
                
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_pb_p15.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_pb_p15" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="ab_08" data-page-number="08" data-type="content" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p08">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
                
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_ab_p08.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_l1_ab_p08" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="ab_09" data-page-number="09" data-type="content" data-book="activity" data-page-id="cb869919f999922css_ae_l1_ab_p09">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 60px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="2ca81498a456e4f4657c5f56b5084bf2" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_10.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_f658131504e3e1df6ece524771f8e46f">

        
    	 <p>pencil<br></p><p>bag</p><p>book</p><p>chair</p><p>Ooooh! Look, it's my pencil!<br></p></div>

            </div>
            </div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_ab_p09.JvcfNRk5"><canvas id="page-canvas-cb869919f999922css_ae_l1_ab_p09" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="ab_10" data-page-number="10" data-type="content" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p10">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 60px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="09e7709ed48738c7fb547030d54075e8" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_12.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_e82900d168fcfd065df53ef29f6fe5b5">

        
    	 <p>Hands down<br></p></div>

            </div>
            </div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_ab_p10.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_l1_ab_p10" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="ab_11" data-page-number="11" data-type="content" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p11">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
                
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_ab_p11.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_l1_ab_p11" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="ab_12" data-page-number="12" data-type="content" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p12">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 50px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="becca04890f925be2083f191e84f3a7e" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_16.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_eb756a212ddb8b8f0f54cb11da7479c3">

        
    	 <p>MIKE: It's my chair.<br></p></div>

            </div>
            </div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_ab_p12.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_l1_ab_p12" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="ab_13" data-page-number="13" data-type="content" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p13">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
                
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_ab_p13.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_l1_ab_p13" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="ab_14" data-page-number="14" data-type="content" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p14">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
                
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_ab_p14.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_l1_ab_p14" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="ab_15" data-page-number="15" data-type="content" data-book="activity" data-page-id="cb868818f898822css_ae_l1_ab_p15">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
                
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_ab_p15.JvcfNRk5"><canvas id="page-canvas-cb868818f898822css_ae_l1_ab_p15" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        
    <div id="notepad-container" touch-action="none">
        <canvas id="notepad-canvas-1" class="notepad-canvas"></canvas><canvas id="notepad-canvas-2" class="notepad-canvas"></canvas><canvas id="notepad-canvas-3" class="notepad-canvas"></canvas><span class="sprite close-popup"></span>
        <div id="notepad-breadcrumb">
            <span class="current-page">1</span> of 3
        </div>
        <span class="sprite nav-button active" id="notepad-nav-prev"></span>
        <span class="sprite nav-button active" id="notepad-nav-next"></span>
    </div>
    <div id="draw-toolbar" class="dragable expanded active">
        <div class="sprite toggle"></div>
        <div class="draw-toolbar-content">
            <div class="sprite drag"></div>
            <div id="draw-tools-wrap">
                <div class="sprite draw-tool pointer" data-tool="pointer" title=""></div>
                <div class="sprite draw-tool pencil" data-tool="pencil" title="Pen">
                    <div class="submenu pencil">
                        <div class="submenu-item pencil-stroke small" data-size="6"></div>
                        <div class="submenu-item pencil-stroke default" data-size="12"></div>
                        <div class="submenu-item pencil-stroke big" data-size="40"></div>
                    </div>
                </div>
                <div class="sprite draw-tool marker" data-tool="marker" title="Highlighter">
                    <div class="submenu marker">
                        <div class="submenu-item marker-stroke small" data-size="6"></div>
                        <div class="submenu-item marker-stroke default" data-size="12"></div>
                        <div class="submenu-item marker-stroke big" data-size="40"></div>
                    </div>
                </div>
                <div class="sprite draw-tool rubber" data-tool="rubber" title="Eraser">
                    <div class="submenu rubber">
                        <div class="sprite submenu-item clear-all"></div>
                        <div class="sprite submenu-item clear-line"></div>
                    </div>
                </div>
                <div class="sprite draw-tool palette" data-tool="palette" title="Colour">
                    <div class="submenu palette">
                        <div class="submenu-item color" data-color="#fff" style="background-color:#fff"></div>
                        <div class="submenu-item color" data-color="#000" style="background-color:#000"></div>
                        <div class="submenu-item color" data-color="#ec6f34" style="background-color:#ec6f34"></div>
                        <div class="submenu-item color" data-color="#EC1E24" style="background-color:#EC1E24"></div>
                        <div class="submenu-item color" data-color="#FCF204" style="background-color:#FCF204"></div>
                        <div class="submenu-item color" data-color="#3CB64C" style="background-color:#3CB64C"></div>
                        <div class="submenu-item color" data-color="#04AEEC" style="background-color:#04AEEC"></div>
                        <div class="submenu-item color" data-color="#242264" style="background-color:#242264"></div>
                        <div class="submenu-item color" data-color="#951D87" style="background-color:#951D87"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="item-container">
        <span class="sprite close-item-container"></span>
        <div id="item-container-placeholder"></div>
    </div>

    <div id="activity-overlay" class="hidden">
        <div id="activity-placeholder"></div>
        <span class="sprite close-activity"></span>
    </div>

    <div id="audio-resource-player" class="hidden">
        <div class="header">
            <span class="sprite drag"></span>
            <span class="sprite close"></span>
        </div>
        <div class="body">
            <div id="audio-player-wrap"></div>
            <div id="transcript-buttons" class="audio">
                <button id="toggle-transcript">Show transcript</button>
                <button id="transcript-highlight" class="sprite hidden"></button>
            </div>
            <div id="transcript-wrap" class="hidden"></div>
        </div>
    </div>

    <div id="answer-key-widget" class="hidden">
        <div class="header">
            <span class="sprite key"></span>
            <span class="sprite close"></span>
            <span class="sprite toggle"></span>
        </div>
        <div class="body">
        </div>
        <div class="footer">
            <button id="show-all-keys" class="button button-small active">Show all</button>
            <button id="next-key" class="button button-small active">Next Item</button>
            <button id="prev-key" class="button button-small active">Last Item</button>
        </div>
    </div>

    <input type="file" class="hidden" id="attachment-upload"></div>
<div id="dialog-overlay" class="hidden">
    <div id="dialog-placeholder"></div>
</div>



<noscript data-type="dialog" id="delete-note-dialog-template">
    <div id="delete-note-dialog">
        Are you sure that you want to delete this note? <br><div class="buttons-wrap">
            <button id="delete-note-button-accept" class="button">Yes</button>
            <button id="delete-note-button-cancel" class="button">No</button>
        </div>
    </div>
</noscript>

<noscript data-type="dialog" id="delete-attachment-dialog-template">
    <div id="delete-attachment-dialog">
        Are you sure that you want to delete this attachment? <br><div class="buttons-wrap">
            <button id="delete-attachment-button-accept" class="button">Yes</button>
            <button id="delete-attachment-button-cancel" class="button">No</button>
        </div>
    </div>
</noscript>

<noscript data-type="dialog" id="attachment-file-size-exceded-dialog-template">
    <div id="delete-note-dialog">
        Max file size exceded (1.5Mb)<br><div class="buttons-wrap">
            <button id="generic-button-accept" class="button">Ok</button>
        </div>
    </div>
</noscript>

<noscript data-type="dialog" id="attachment-file-size-exceded-dialog-template">
    <div id="delete-note-dialog">
        Unexpected error saving your file. Filoe too large?<br><div class="buttons-wrap">
            <button id="generic-button-accept" class="button">Ok</button>
        </div>
    </div>
</noscript>

<noscript data-type="dialog" id="attachment-database-error-dialog-template">
    <div id="delete-note-dialog">
        Error accessing local database. Please try again<br><div id="custom-html-placeholder"></div>
        <div class="buttons-wrap">
            <button id="generic-button-accept" class="button">Ok</button>
        </div>
    </div>
</noscript>

<noscript data-type="dialog" id="attachment-not-supported-dialog-template">
    <div id="delete-note-dialog">
        Attachment functionality is not supported on this device.<br><div id="custom-html-placeholder"></div>
        <div class="buttons-wrap">
            <button id="generic-button-accept" class="button">Ok</button>
        </div>
    </div>
</noscript>

<noscript id="audio-player-template">
    <div class="media presentationPlus ">
        <div class="media-body">
                        <div class="netex-audio-player" data-audio-swf-url="./netexAudio.swf" data-audio-player-id="audio_">
                <div class="netex-audio-container">
                    <audio preload="none"><source src="" type="audio/ogg"></source></audio></div>
                <div class="netex-audio-controls-container">
                    <div class="netex-audio-controls">
                        <div class="netex-audio-controls-item netex-audio-button-play-pause-restart netex-audio-button-play"></div>
                        <div class="netex-audio-time-container">
                            <span class="netex-audio-time-current">00:00</span>
                            <div class="netex-audio-progress-bar-container">
                                <div class="netex-audio-progress-bar">
                                    <div class="netex-audio-progress-bar-inner"></div>
                                </div>
                            </div>
                            <span class="netex-audio-time-duration">00:00</span>
                        </div>
                        <div class="netex-audio-volume-control">
                                <div class="netex-audio-volume-slider netex-audio-volume-draggable">
                                    <div class="netex-audio-volume-slider-foreground"></div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</noscript>



<noscript data-type="activity" data-id="activity_013d3a3e63c4ce3d6e22c073c30506f6">
<div>





          <div class="container" style="height:610px; width:740px; position:relative; margin:0 auto;">
            <div id="test_4d55a53b-0bd8-4fbc-9e34-d5bcd641e3af" class="testLC_KidsBox testLCComponent" style="height:610px;">
                <div id="4cf81ff3c561894534fa4d7a1491edf7_N1TkBq" class="itemTestLC_KidsBox itemTestLCComponent" style="height: 610px; width: 740px; top: 0px; left: 0px;" data-activity-type="T15-Pelmanism" data-type="T15-Pelmanism">
                    <div class="marcoKidsBox " style="height: 567px;"></div>
                    <div class="copyrightKidsBox "></div>
                    <div class="tapaActivity" style="height: 435px;"></div>
                    <div class="enviarComponent"><p>Save</p></div>
                    <div class="disableActivity" style="height: 435px;"></div>
                    <div class="solutionComponent"><p>


See answers                    </p></div>

                    <div class="reiniciaComponent desactive"></div>
                    <div class="comprobarComponent" intentos="2"><p>
Check                    </p></div>
                      <div class="nuevoIntentoComponent"><p>
Try again                    </p></div>

                        <div class="enunciado_KidsBox enunciadoComponent" style="height:48px; width:524.85px; top:20px; left:30px; position:absolute;">
                                                           <p>
<b>Find the pairs.</b>                                    </p>
                        </div>

                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:100px; left:100px; position:absolute;" id="LBgI6G" correcto="iXIHS5" urlmp3="">
                            <div class="carta_v2 divHighlight" id="LBgI6G_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="LBgI6G_img" src="resources/activities/ss_l1_u1_ex1_01.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/ss_l1_u1_ex1_01.JvcfNRk5"></div>



                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:100px; left:247px; position:absolute;" id="CXgBGm" correcto="n4fiLO" urlmp3="">
                            <div class="carta_v2 divHighlight" id="CXgBGm_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="CXgBGm_img" src="resources/activities/ss_l1_u1_ex1_02.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/ss_l1_u1_ex1_02.JvcfNRk5"></div>



                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:100px; left:394px; position:absolute;" id="P6T68o" correcto="c6T23p" urlmp3="">
                            <div class="carta_v2 divHighlight" id="P6T68o_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="P6T68o_img" src="resources/activities/ss_l1_u1_ex1_03.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/ss_l1_u1_ex1_03.JvcfNRk5"></div>



                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:100px; left:540px; position:absolute;" id="MeSDD1" correcto="y8wNpV" urlmp3="">
                            <div class="carta_v2 divHighlight" id="MeSDD1_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="MeSDD1_img" src="resources/activities/ss_l1_u1_ex1_04.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/ss_l1_u1_ex1_04.JvcfNRk5"></div>



                        <div class="feedback_KidsBox feedbackComponent act_positivo" style="display:none;position:absolute;">
                                    <p>Well done!</p>
                                    <div class="closeButton"></div>
                        </div>
                        <div class="feedback_KidsBox feedbackComponent act_medio" style="display:none;position:absolute;">
                                    <p>Try again!</p>
                                    <div class="closeButton"></div>
                        </div>
                        <div class="feedback_KidsBox feedbackComponent act_negativo" style="display:none; position:absolute;">
                                    <p>Check the correct answers.</p>
                                    <div class="closeButton"></div>
                        </div>

                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:300px; left:100px; position:absolute;" id="iXIHS5" correcto="LBgI6G" urlmp3="resources/activities/ss_l1_u1_bag.a2fWF5NXu">
                            <div class="carta_v2 divHighlight" id="iXIHS5_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="iXIHS5_img" src="resources/activities/audio.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/audio.JvcfNRk5"></div>



                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:300px; left:247px; position:absolute;" id="n4fiLO" correcto="CXgBGm" urlmp3="resources/activities/ss_l1_u1_pencil.a2fWF5NXu">
                            <div class="carta_v2 divHighlight" id="n4fiLO_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="n4fiLO_img" src="resources/activities/audio.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/audio.JvcfNRk5"></div>



                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:300px; left:394px; position:absolute;" id="c6T23p" correcto="P6T68o" urlmp3="resources/activities/ss_l1_u1_chair.a2fWF5NXu">
                            <div class="carta_v2 divHighlight" id="c6T23p_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="c6T23p_img" src="resources/activities/audio.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/audio.JvcfNRk5"></div>



                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:300px; left:540px; position:absolute;" id="y8wNpV" correcto="MeSDD1" urlmp3="resources/activities/ss_l1_u1_book.a2fWF5NXu">
                            <div class="carta_v2 divHighlight" id="y8wNpV_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="y8wNpV_img" src="resources/activities/audio.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/audio.JvcfNRk5"></div>




                </div>
            </div>
        </div>


</div>


<script>if( typeof funcionalityActivity == 'function'){funcionalityActivity()};if( typeof isSimon == 'function'){ Simon.Main.init();}</script></noscript>

<noscript data-type="activity" data-id="activity_4d3f252782cdc28cc168e7fb1e7d9a2b">
<div>

<div id="simon" class="columns_2" data-allow-repeated-items="no" data-secuence-size="4" data-max-lifes=" 3">
    
        <div id="simon-header">
        <div class="rubric">
        
    <p><strong>Listen and choose in order.</strong><br></p></div>


        <button id="play-btn" class="header-item sprite"></button>
        <div id="score" class="header-item">
            <!--<span id="score-text" class="sprite"></span>-->  
            <span id="score-number"></span>  
        </div>
        <ul id="lifes-counter" class="header-item"><li class="life-icon sprite" data-position="0">

                </li>
                            <li class="life-icon sprite" data-position="1">

                </li>
                            <li class="life-icon sprite" data-position="2">

                </li>
                    </ul><!--<button id="restart-btn" class="sprite"></button>--></div>
  
    <div id="simon-content">
        <div>
            
<div id="card-97c766882d9cce5a094fd5aeb9ef586e" class="card" data-audio-src="resources/ss_l1_u1_chair.a2fWF5NXu"> 
    <img src="resources/ss_l1_u1_ex2_01.JvcfNRk5" class="card-image" width="100%" height="100%"><div class="card-overlay"></div>
</div>

<div id="card-261ff8927fdd4632dab2919c7e5a9425" class="card" data-audio-src="resources/ss_l1_u1_pencil.a2fWF5NXu"> 
    <img src="resources/ss_l1_u1_ex2_02.JvcfNRk5" class="card-image" width="100%" height="100%"><div class="card-overlay"></div>
</div>

<div id="card-2b166cb462d4f87ba22693e39f2ef285" class="card" data-audio-src="resources/ss_l1_u1_bag.a2fWF5NXu"> 
    <img src="resources/ss_l1_u1_ex2_03.JvcfNRk5" class="card-image" width="100%" height="100%"><div class="card-overlay"></div>
</div>

<div id="card-a8551f13b94650b2b853dc34574d25fc" class="card" data-audio-src="resources/ss_l1_u1_book.a2fWF5NXu"> 
    <img src="resources/ss_l1_u1_ex2_04.JvcfNRk5" class="card-image" width="100%" height="100%"><div class="card-overlay"></div>
</div>
        </div>
    </div>
    <div class="copyright">
        <h3></h3>
    </div>
    <div id="feedback-ok" class="feedback hidden ">
    </div>
    <div id="feedback-ko" class="feedback hidden ">
    </div>
    <div id="feedback-done" class="feedback hidden ">
    </div>
    <div id="feedback-game-over" class="feedback hidden ">
    </div>
    <audio id="audio-player"></audio><audio id="audioSprite" preload="auto"><source src="commoncontents/audio/audios.a2fWF5NXu" type="audio/ogg"></source></audio></div>

</div>


<script>if( typeof funcionalityActivity == 'function'){funcionalityActivity()};if( typeof isSimon == 'function'){ Simon.Main.init();}</script></noscript>

<noscript data-type="activity" data-id="activity_63c9736cd4bf28b1f320e5a85a3f4ef1">
<div>





          <div class="container" style="height:1175px; width:740px; position:relative; margin:0 auto;">
            <div id="test_a506eceb-845b-4b3b-a79a-b20a4497aa30" class="testLC_KidsBox testLCComponent" style="height:1175px;">
                <div id="ef53b8bf268b6d97dbe09163037df30f_hB6wrg" class="itemTestLC_KidsBox itemTestLCComponent" style="height: 1175px; width: 740px; top: 0px; left: 0px;" data-activity-type="T0-Presentation" data-type="T0-Presentation">
                    <div class="marcoKidsBox " style="height: 1132px;"></div>
                    <div class="copyrightKidsBox "></div>
                    <div class="tapaActivity" style="height: 1000px;"></div>
                    <div class="enviarComponent"><p>Save</p></div>
                    <div class="disableActivity" style="height: 1000px;"></div>
                    <div class="solutionComponent"><p>


See answers                    </p></div>

                    <div class="reiniciaComponent desactive"></div>
                    <div class="comprobarComponent" intentos="2"><p>
Check                    </p></div>
                      <div class="nuevoIntentoComponent"><p>
Try again                    </p></div>

                            <div style="height:53px; width:174px; top:30px; left:30px; position:absolute; z-index:991">
                                <audio class="audioKidsBox" controls="controls" preload="none"><source type="audio/ogg" src="resources/activities/super_safari_l1_cd1_15.a2fWF5NXu"></source></audio></div>



                        <div class="elementoExterno_v2 elementoExtComponent" style="height:1024px; width:731px; top:99px; left:5px; position:absolute;">

                                <img src="resources/activities/ss_l1_u1_ex3.JvcfNRk5" nombre="imagen_spYHoU;" style="height:1024px; width:731px;"></div>


                </div>
            </div>
        </div>


</div>


<script>if( typeof funcionalityActivity == 'function'){funcionalityActivity()};if( typeof isSimon == 'function'){ Simon.Main.init();}</script></noscript>




                  <script src="commoncontents/ntxtmp/js/libs/jquery-2.1.1.min.js"></script><script src="trackingConfig.js"></script><script src="commoncontents/kidsBox/js/libs/mediaelement/mediaelement-and-player.js"></script><script src="commoncontents/kidsBox/js/netex/htmlActivity_min.js"></script><script src="trackingConfig.js"></script><script src="commoncontents/js/lib/underscore-min.js"></script><script src="commoncontents/js/lib/backbone-min.js"></script><script src="commoncontents/js/lib/pointerEvents.js"></script><script src="commoncontents/js/simon.min.js"></script><script src="trackingConfig.js"></script><script src="commoncontents/ntxtmp/js/libs/pointerEvents.js"></script><script src="commoncontents/ntxtmp/js/netex/ntxtmp_min.js"></script><script src="commoncontents/ntxtmp/js/netex/model/dbModel.js"></script><style>
                .Wirisformula:not([width]) {
                    vertical-align: middle !important;
                    margin-bottom: -8px !important;
                }
            </style><style>
                .mathquill_image {
                    vertical-align: middle !important;
                    margin-top: -3px !important;
                }
            </style></body></html>
