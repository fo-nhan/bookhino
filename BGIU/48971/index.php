<?php
session_start();

require './config.php';
require './request.php';

if (isset($_SESSION['token']) && isset($_SESSION['user_id']) && $_SESSION['token'] && $_SESSION['user_id']) {
    $sql = "SELECT * FROM access WHERE `access`.`id` = ? AND `access`.`token` = ? AND `access`.`status` = 'normal' ";
    $request = REQUEST_GET($sql, "ss", [$_SESSION['user_id'], $_SESSION['token']]);
    if ($request['status'] !== 200) {
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s', strtotime('+5 minutes', strtotime($startTime)));
        $sql4 = "UPDATE `access` SET `timeEnd` = ? WHERE `access`.`id` = ? AND `access`.`token` = ? AND `access`.`status` = 'normal'";
        $request4 = REQUEST_POST($sql4, "sss", [$cenvertedTime, $_SESSION['user_id'], $_SESSION['token']]);
        session_unset();
        session_destroy();
        echo '<script> alert("You are logged in somewhere else!"); </script> ';
        echo '<script> window.location="/login.php"; </script> ';
    }
} else {
    session_unset();
    session_destroy();
    echo '<script> window.location="/login.php"; </script> ';
}
if (!isset($_SESSION['book1'])) {

    echo '<script> window.location="/login.php"; </script> ';
} else if ($_SESSION['book1'] == 0) {
    echo '<script> window.location="/index.php"; </script> ';
}

?>
<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="apple-mobile-web-app-capable" content="yes"><meta name="apple-mobile-web-app-status-bar-style" content="black"><meta name="viewport" content="target-densitydpi=device-dpi, width=device-width"><!--<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">--><meta name="author" content="Netex"><meta name="dcterms.created" content="05-02-2016 08:35:55"><!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=Edge"><![endif]--><link data-move-head="true" rel="stylesheet" href="commoncontents/kidsBox/js/libs/mediaelement/mediaelementplayer.min.css"><link data-move-head="true" rel="stylesheet" href="commoncontents/kidsBox/css/kidsBox_min.css"><link data-move-head="true" rel="stylesheet" href="commoncontents/kidsBox/js/netex/htmlActivity/css/testLC_kidsBox_min.css"><link data-move-head="true" rel="stylesheet" href="commoncontents/kidsBox/css/translate.css"><link data-move-head="true" rel="stylesheet" href="commoncontents/css/simon.min.css"><title>Presentation plus</title><link rel="stylesheet" href="commoncontents/ntxtmp/css/libs/jquery-ui.min.css"><link rel="stylesheet" href="commoncontents/ntxtmp/css/libs/normalize.css"><link rel="stylesheet" href="commoncontents/ntxtmp/css/libs/animate.min.css"><link rel="stylesheet" href="commoncontents/ntxtmp/js/netex/plugins/video/netexVideoSkin.css"><link rel="stylesheet" href="commoncontents/ntxtmp/js/netex/plugins/audio/netexAudioSkin.css"><link rel="stylesheet" href="commoncontents/ntxtmp/css/netex/ntxtmp_min.css"><meta name="learningmaker-version" content="4.2.3.10"></head><body data-course-id="CODE-48971" class="teacher-package">

<script>
    window.addEventListener("click", () => {
      const token = localStorage.getItem('token');
      if (token) {
        fetch('checkpoint.php', {
            method: 'GET', // or 'PUT'
            headers: {
              'Content-Type': 'application/json',
            },
          })
          .then((response) => response.json())
          .then((data) => {
            if(data && data.checkpoint){
              alert("You are logged in somewhere else!");
              <?php echo 'window.location="/login.php"; '; ?>
            }
          })
      }
    })
  </script>


<div id="page-wrap" class="landscape">
    <noscript id="gaTrackingConfig" data-ga-tracking-id="UA-8600104-1" data-ga-tracking-product-name="presentationplus" data-ga-tracking-course-name="supersafariAE" data-ga-tracking-level-name="level1" data-ga-tracking-unit-name=""></noscript>

<div id="main-menu-wrap">
    <ul class="main-menu enabled" id="main-menu"><li class="main-menu-item home active">  
            <a href="../index.php" title="Home" class="sprite"></a>
        </li>
        <li class="main-menu-item pupil-book active">
            <span class="sprite" title="Student's Book"></span>
            <ul class="navigator pupil-book-navigator"></ul></li>
        <li class="main-menu-item activity-book">
           <span class="sprite" title="Workbook"></span>
            <ul class="navigator activity-book-navigator"></ul></li>
        <li class="main-menu-item media">
            <span class="sprite" title="Media Library"></span>
            <ul class="sub-menu" id="sub-menu-media"><li class="sub-menu-item video-popup">    
                    <span class="sprite" title="Videos"></span>
                </li>
                <li class="sub-menu-item audio-popup">   
                    <span class="sprite" title="Extra Audio"></span>
                </li>
                <li class="sub-menu-item flashcard-popup">    
                    <span class="sprite" title="Flashcards"></span>
                </li>
            </ul></li>
        <li class="main-menu-item resource-popup">
            <span class="sprite" title="Resources"></span>
        </li>
        <li class="main-menu-item link-popup active">
            <span class="sprite" title="Links"></span>
        </li>
        <li class="main-menu-item bookmark active">
            <span class="sprite" title="Bookmarks"></span>
            <ul class="sub-menu" id="sub-menu-bookmark"><li class="sub-menu-item return active">   
                    <span class="sprite" title="Return"></span>
                </li>
                <li class="sub-menu-item add active">  
                    <span class="sprite" title="Add bookmark"></span>
                </li>
                <li class="sub-menu-item list-popup active">   
                    <span class="sprite" title="Bookmark list"></span>
                </li>
            </ul></li>
        <li class="main-menu-item note active">
            <span class="sprite" title="Notes"></span>
            <ul class="sub-menu" id="sub-menu-note"><li class="sub-menu-item add-note active">      
                    <span class="sprite" title="Add Note"></span>
                </li>
                <li class="sub-menu-item add-attachment active">    
                    <span class="sprite" title="Add Attachment"></span>
                </li>
            </ul></li>
        <li class="main-menu-item mask active">
            <span class="sprite" title="Mask"></span>
        </li>
        <li class="main-menu-item notepad active">
            <span class="sprite" title="Notepad"></span>
        </li>
    </ul><div id="title-bar">
        Unit 0 - Hello!                    <span class="sprite answer-key-toggle" title="Answer Key"></span>
                <span class="sprite zoom zoom-in" title="Zoom In"></span>
        <div id="answer-key-nav">
            <ul></ul></div>
    </div>
    
</div>
    

 
    <noscript data-type="popup" data-id="video-popup"> 

        <div class="popup video-popup">
            <span class="sprite close-popup"></span>
            <h2 class="popup-title">Video</h2>

            <div id="video-player-wrap">
                <div id="video-player-placeholder"></div>
                <div id="transcript-buttons" class="video hidden">
                    <button id="toggle-transcript">Show transcript</button>
                    <button id="transcript-highlight" class="sprite hidden"></button>
                </div>
            </div>
            <div id="video-gallery-wrap">
                <ul id="video-gallery"><li>
                                    <img class="video-thumb" data-id="video_6697cfbda887522fc43f8e96245e3a97" src="resources/videos/u0_song.JvcfNRk5"></li>
                                                                                   
                                <li>
                                    <img class="video-thumb" data-id="video_5d11b819fbea57c417d6b7bf3f367670" src="resources/videos/u0_karaoke.JvcfNRk5"></li>
                                                                                        </ul></div>
            <div id="transcript-wrap" class="hidden"></div>
        </div>
    </noscript> 

           

    <noscript data-type="video" id="video_6697cfbda887522fc43f8e96245e3a97">
        
        <div class="media">
            <div class="media-body">
                                <div class="netex-video-player" data-video-swf-url="./netexVideo.swf" data-video-player-id="video_6697cfbda887522fc43f8e96245e3a97" data-height="576" data-width="720">
                    <div class="netex-video-responsive" style="padding-bottom:80%;">
                        <div class="netex-video-container">
                                                                        <div class="mejs-overlay mejs-layer mejs-overlay-play" style="width: 100%; height: 100%;">
                            <div class="mejs-overlay-button" style="margin-top: -35px;"></div>                                        
                        </div>
                        <video preload="none" webkit-playsinline="1"><source src="resources/videos/u0_song.MvTDKVFm" type="video/webm"></source></video><img src="resources/videos/u0_song.JvcfNRk5" class="netex-video-poster"></div>
                    </div>
                    <div class="netex-video-controls-container">
                        <div class="netex-video-progress-bar-container">
                            <div class="netex-video-progress-bar">
                                <div class="netex-video-progress-bar-inner"></div>
                            </div>
                        </div>
                        <div class="netex-video-controls">
                            <div class="netex-video-controls-item netex-video-button-play-pause-restart netex-video-button-play"></div>
                            <div class="netex-video-volume-control"><div class="netex-video-controls-item netex-video-volume-button netex-video-volume-large"></div><div class="netex-video-volume-panel"><div class="netex-video-volume-slider netex-video-volume-draggable"><div class="netex-video-volume-slider-foreground"></div></div></div></div>
                            <div class="netex-video-time-container"><span class="netex-video-time-current">00:00</span><span class="netex-video-time-separator"> / </span><span class="netex-video-time-duration">00:00</span></div>
                            <div class="netex-video-tools-item netex-video-button-fullscreen netex-video-button-fullscreen-enter" title="Fullscreen On"></div>
                                                    </div>
                    </div>
                </div>
            </div>
        </div>
            </noscript>
           

    <noscript data-type="video" id="video_5d11b819fbea57c417d6b7bf3f367670">
        
        <div class="media">
            <div class="media-body">
                                <div class="netex-video-player" data-video-swf-url="./netexVideo.swf" data-video-player-id="video_5d11b819fbea57c417d6b7bf3f367670" data-height="576" data-width="720">
                    <div class="netex-video-responsive" style="padding-bottom:80%;">
                        <div class="netex-video-container">
                                                                        <div class="mejs-overlay mejs-layer mejs-overlay-play" style="width: 100%; height: 100%;">
                            <div class="mejs-overlay-button" style="margin-top: -35px;"></div>                                        
                        </div>
                        <video preload="none" webkit-playsinline="1"><source src="resources/videos/u0_karaoke.MvTDKVFm" type="video/webm"></source></video><img src="resources/videos/u0_karaoke.JvcfNRk5" class="netex-video-poster"></div>
                    </div>
                    <div class="netex-video-controls-container">
                        <div class="netex-video-progress-bar-container">
                            <div class="netex-video-progress-bar">
                                <div class="netex-video-progress-bar-inner"></div>
                            </div>
                        </div>
                        <div class="netex-video-controls">
                            <div class="netex-video-controls-item netex-video-button-play-pause-restart netex-video-button-play"></div>
                            <div class="netex-video-volume-control"><div class="netex-video-controls-item netex-video-volume-button netex-video-volume-large"></div><div class="netex-video-volume-panel"><div class="netex-video-volume-slider netex-video-volume-draggable"><div class="netex-video-volume-slider-foreground"></div></div></div></div>
                            <div class="netex-video-time-container"><span class="netex-video-time-current">00:00</span><span class="netex-video-time-separator"> / </span><span class="netex-video-time-duration">00:00</span></div>
                            <div class="netex-video-tools-item netex-video-button-fullscreen netex-video-button-fullscreen-enter" title="Fullscreen On"></div>
                                                    </div>
                    </div>
                </div>
            </div>
        </div>
            </noscript>
    
 
    <noscript data-type="popup" data-id="audio-popup"> 
        <div class="popup audio-popup">
                <span class="sprite close-popup"></span>
            <h2 class="popup-title">Audio</h2>
                <ul id="audio-gallery"><div class="audio-gallery-row">
                                                                           
                                <li>
                                    <span class="audio-title" data-id="audio_d96a8bbeafad7ca75de7256889f75631">
                                         <span class="sprite audio-bullet"></span>
                                        Hello! Chant                                    </span>
                                </li>
                                                                                                               
                                <li>
                                    <span class="audio-title" data-id="audio_ef9c44b25ff7140144910dcfdcd1cbf1">
                                         <span class="sprite audio-bullet"></span>
                                        Hello! Song                                    </span>
                                </li>
                                                                                                                        </div>
                </ul><div id="audio-player-wrap"></div>
                <div id="transcript-buttons" class="audio hidden">
                    <button id="toggle-transcript">Show transcript</button>
                    <button id="transcript-highlight" class="sprite hidden"></button>
                </div>
                <div id="transcript-wrap" class="hidden"></div>
        </div>
    </noscript> 
    

    
                       
                <noscript data-type="audio" id="audio_d96a8bbeafad7ca75de7256889f75631">
                    <div class="media presentationPlus">
                        <div class="media-body">
                                                        <div class="netex-audio-player" data-audio-swf-url="./netexAudio.swf" data-audio-player-id="audio_d96a8bbeafad7ca75de7256889f75631">
                                <div class="netex-audio-container">
                                    <audio preload="none"><source src="resources/pop_audio/0a.a2fWF5NXu" type="audio/ogg"></source></audio></div>
                                <div class="netex-audio-controls-container">
                                    <div class="netex-audio-controls">
                                        <div class="netex-audio-controls-item netex-audio-button-play-pause-restart netex-audio-button-play"></div>
                                        <div class="netex-audio-time-container">
                                            <span class="netex-audio-time-current">00:00</span>
                                            <div class="netex-audio-progress-bar-container">
                                                <div class="netex-audio-progress-bar">
                                                    <div class="netex-audio-progress-bar-inner"></div>
                                                </div>
                                            </div>
                                            <span class="netex-audio-time-duration">00:00</span>
                                        </div>
                                        <div class="netex-audio-volume-control">
                                                <div class="netex-audio-volume-slider netex-audio-volume-draggable">
                                                    <div class="netex-audio-volume-slider-foreground"></div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="transcript hidden  " id="transcript_0766b29c001ab2422cac9bb3274c1372">

        
    	 <p>Hello, hello, hello!</p><p>I'm Jo, I'm Jo, I'm Jo!</p><p>Hello, hello, hello!</p><p>I'm Jo, I'm Jo, I'm Jo!<br></p><p><br></p><p>Hello, hello, hello!</p><p>I'm Mo, I'm Mo, I'm Mo!</p><p>Hello, hello, hello!</p><p>I'm Mo, I'm Mo, I'm Mo!</p></div>

                </noscript>
                        

    
                       
                <noscript data-type="audio" id="audio_ef9c44b25ff7140144910dcfdcd1cbf1">
                    <div class="media presentationPlus">
                        <div class="media-body">
                                                        <div class="netex-audio-player" data-audio-swf-url="./netexAudio.swf" data-audio-player-id="audio_ef9c44b25ff7140144910dcfdcd1cbf1">
                                <div class="netex-audio-container">
                                    <audio preload="none"><source src="resources/pop_audio/0b.a2fWF5NXu" type="audio/ogg"></source></audio></div>
                                <div class="netex-audio-controls-container">
                                    <div class="netex-audio-controls">
                                        <div class="netex-audio-controls-item netex-audio-button-play-pause-restart netex-audio-button-play"></div>
                                        <div class="netex-audio-time-container">
                                            <span class="netex-audio-time-current">00:00</span>
                                            <div class="netex-audio-progress-bar-container">
                                                <div class="netex-audio-progress-bar">
                                                    <div class="netex-audio-progress-bar-inner"></div>
                                                </div>
                                            </div>
                                            <span class="netex-audio-time-duration">00:00</span>
                                        </div>
                                        <div class="netex-audio-volume-control">
                                                <div class="netex-audio-volume-slider netex-audio-volume-draggable">
                                                    <div class="netex-audio-volume-slider-foreground"></div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="transcript hidden  " id="transcript_8f9ec5ee0b4758019fa24d37fb7cc9a2">

        
    	 <p>Clap your hands,</p><p>Clap your hands,<br></p><p>Let's go, let's go.<br></p><p>And say “Hello”!<br></p><p><br></p><p>Hello, hello,<br></p><p>Hello, hello,<br></p><p>Hello, hello,<br></p><p><br></p><p>Hello!<br></p><p><br></p><p>Stand up,<br></p><p>Sit down,<br></p><p>Let's go, let's go.<br></p><p>And say “Hello”!<br></p><p><br></p><p>Hello, hello, ...<br></p><p><br></p><p>Clap your hands,<br></p><p>Clap your hands,<br></p><p>Let's go, let's go.</p><p>And say “Hello”!<br></p><p><br></p><p>Hello, hello, ...<br></p><p><br></p><p>Stand up,<br></p><p>Sit down,<br></p><p>Let's go, let's go.<br></p><p>And say “Hello”!</p><p>Hello, hello, ...</p></div>

                </noscript>
                          
    <noscript data-type="popup" data-id="flashcard-popup"> 
<div class="popup flashcard-popup">
        <span class="sprite close-popup"></span>
    <h2 class="popup-title">Flashcards</h2>
    
    <div class="carousel">
        <span class="sprite nav nav-prev enabled" id="prev-flashcard"></span>
        <div class="carousel-content-wrap">
            <ul class="carousel-content"><li class="flashcard-thumb">
		<img src="resources/flashcards/ss_ae_l1_fc_p02_th.JvcfNRk5" data-type="thumb" data-id="854fe57d41a8cbeess_ae_l1_fc_p02" data-title="Gina" data-rel-fullscreen="resources/flashcards/ss_ae_l1_fc_p02.JvcfNRk5"></li>

			<li class="flashcard-thumb">
		<img src="resources/flashcards/ss_ae_l1_fc_p04_th.JvcfNRk5" data-type="thumb" data-id="854fe57d41a8cbeess_ae_l1_fc_p04" data-title="Polly" data-rel-fullscreen="resources/flashcards/ss_ae_l1_fc_p04.JvcfNRk5"></li>

			<li class="flashcard-thumb">
		<img src="resources/flashcards/ss_ae_l1_fc_p06_th.JvcfNRk5" data-type="thumb" data-id="854fe57d41a8cbeess_ae_l1_fc_p06" data-title="Leo" data-rel-fullscreen="resources/flashcards/ss_ae_l1_fc_p06.JvcfNRk5"></li>

			<li class="flashcard-thumb">
		<img src="resources/flashcards/ss_ae_l1_fc_p08_th.JvcfNRk5" data-type="thumb" data-id="854fe57d41a8cbeess_ae_l1_fc_p08" data-title="Mike" data-rel-fullscreen="resources/flashcards/ss_ae_l1_fc_p08.JvcfNRk5"></li>

            </ul></div>
        <span class="sprite nav nav-next enabled" id="next-flashcard"></span>
    </div>
</div>
 
    </noscript> 
  
    
    <noscript data-type="popup" data-id="link-popup"> 
<div class="popup link-popup">
        <span class="sprite close-popup"></span>
    <h2 class="popup-title">Links</h2>
    <ul id="list-link"><li class="link">
                <a target="_blank" href="http://www.cambridgelms.org/primary/p/splash">
            Cambridge LMS (online homework)        </a>
    </li>

    <li class="link">
                <a target="_blank" href="http://www.cambridge.org/gb/cambridgeenglish/catalog/primary/super-safari-american-english">
            Super Safari website        </a>
    </li>

    <li class="link">
                <a target="_blank" href="mailto:lms.support@cambridge.org">
            Customer support        </a>
    </li>
    </ul><div class="buttons-wrap">
        <button id="add-link-button" class="button">Add link</button>
    </div>
</div>
 
    </noscript> 

    <noscript data-type="dialog" id="new-link-dialog-template"> 
        <div id="new-link-dialog">
            Link name <br><input type="text" id="link-name" value=""><br>
            Link address <br><input type="text" value="" id="link-address" placeholder="www."><br><div class="buttons-wrap">
                <button id="new-link-button-accept" class="button">Accept</button>
                <button id="new-link-button-cancel" class="button">Cancel</button>
            </div>
        </div>
    </noscript>
        

     
    <noscript data-type="popup" data-id="resource-popup"> 
<div class="popup resource-popup">
        <span class="sprite close-popup"></span>
    <h2 class="popup-title">Resources</h2>
    <ul><li class="resource">
                <a target="_blank" href="resources/resources/pp_help_guide.pdf">
            Help guide 
        </a>
    </li>

 
    
    <li class="resource">
                <a target="_blank" href="resources/resources/tb_u0.pdf">
            Teacher’s Book Hello! 
        </a>
    </li>

 
    
    <li class="resource">
                <a target="_blank" href="resources/resources/tb_intro.pdf">
            Teacher’s Book Introduction 
        </a>
    </li>

 
    
    <li class="resource">
                <a target="_blank" href="resources/resources/sb_extras.pdf">
            Student’s Book certificate and stickers 
        </a>
    </li>

 
    
    <li class="resource">
                <a target="_blank" href="resources/resources/wb_cut_outs.pdf">
            Workbook cutouts 
        </a>
    </li>

 
    
    <li class="resource">
                <a target="_blank" href="resources/resources/phonics_cards.pdf">
            Phonics cards 
        </a>
    </li>
    </ul></div>
 
    </noscript> 

<noscript data-type="popup" data-id="pupil-popup"> 
    <div class="popup pupil-popup ">
        <h2 class="popup-title">Student's Book</h2>
        <div class="carousel">
            <span class="sprite  nav nav-prev enabled" id="prev-book-page"></span>
            <div class="carousel-content-wrap">
                <ul class="carousel-content"><li class="page-thumb" data-book="pupil" data-page-id="cb864418f894422css_ae_l1_pb_p4">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_pb_p04_th.JvcfNRk5"><div class="page-number">
                                Page 04                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="pupil" data-page-id="cb864418f894422css_ae_l1_pb_p05">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_pb_p05_th.JvcfNRk5"><div class="page-number">
                                Page 05                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="pupil" data-page-id="cb864418f894422css_ae_l1_pb_p06">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_pb_p06_th.JvcfNRk5"><div class="page-number">
                                Page 06                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="pupil" data-page-id="cb867718f897722css_ae_l1_pb_p07">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_pb_p07_th.JvcfNRk5"><div class="page-number">
                                Page 07                            </div>
                        </li>
                                    </ul></div>
            <span class="sprite  nav nav-next enabled" id="next-book-page"></span>
        </div>
    </div>
</noscript>
<noscript data-type="popup" data-id="activity-popup"> 
    <div class="popup activity-popup">
        <h2 class="popup-title">Workbook</h2>
        <div class="carousel">
            <span class="sprite  nav nav-prev enabled" id="prev-book-page"></span>
            <div class="carousel-content-wrap">
                <ul class="carousel-content"><li class="page-thumb" data-book="activity" data-page-id="cb864418f894422css_ae_l1_ab_p04">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_ab_p04_th.JvcfNRk5"><div class="page-number">
                                Page 04                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="activity" data-page-id="cb864418f894422css_ae_l1_ab_p05">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_ab_p05_th.JvcfNRk5"><div class="page-number">
                                Page 05                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="activity" data-page-id="cb864418f894422css_ae_l1_ab_p06">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_ab_p06_th.JvcfNRk5"><div class="page-number">
                                Page 06                            </div>
                        </li>
                                                                    <li class="page-thumb" data-book="activity" data-page-id="cb867718f897722css_ae_l1_ab_p07">
                            <span class="sprite page-bookmarked hidden"></span>
                            <img src="resources/paginas/ss_ae_l1_ab_p07_th.JvcfNRk5"><div class="page-number">
                                Page 07                            </div>
                        </li>
                                    </ul></div>
            <span class="sprite  nav nav-next enabled" id="next-book-page"></span>
        </div>
    </div>
</noscript>
<noscript data-type="popup" data-id="bookmark-list-popup"> 
    <div class="popup bookmark-list-popup">
        <span class="sprite close-popup"></span>
        <h2 class="popup-title">Bookmarks</h2>
        <div id="bookmark-list">

        </div>
    </div>
</noscript>


    <div id="popupView" class="ntx-popup"></div>
    <div id="pageView" class="content" touch-action="none" data-trans-out-forward="fadeOut" data-trans-out-rewind="fadeOut" data-trans-in="fadeIn"></div>
    
 <noscript data-page-title="pb_04" data-page-number="04" data-type="content" data-book="pupil" data-page-id="cb864418f894422css_ae_l1_pb_p4">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 130px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="7ea34b858bb45e3327035668bc08759d" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_02.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_a2c1befef83ad25c9b8d5f32405441f4">

        
    	 <p>GINA: Hello, I'm Gina.<br></p><p>POLLY: Hello, I'm Polly.</p><p>LEO: Hello, I'm Leo.</p><p>MIKE: Hello, I'm Mike.</p><p>GINA, POLLY, LEO, MIKE: Hellooo!</p><p><br></p><p>Gina, Polly, Leo, Mike</p></div>

            </div>
            
<div class="sprite activity-resource" rel-activity="activity_e9e2322df4a8dbc2d40654e26fe901dd" title="Activity">
	</div>
<div class="sprite activity-resource" rel-activity="activity_774f8b4e536c8b8ed8796966ae2aa441" title="Activity">
	</div></div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_pb_p04.JvcfNRk5"><canvas id="page-canvas-cb864418f894422css_ae_l1_pb_p4" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="pb_05" data-page-number="05" data-type="content" data-book="pupil" data-page-id="cb864418f894422css_ae_l1_pb_p05">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 50px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="3d59e70670e30142d1e96ad54e6e215b" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_03.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_980279c874c906a6a2179cf63af2d82d">

        
    	 <p>Hello, hello, hello!<br></p><p>I'm Jo, I'm Jo, I'm Jo!</p><p>Hello, hello, hello!</p><p>I'm Jo, I'm Jo, I'm Jo!</p><p>Hello, hello, hello!</p><p>I'm Mo, I'm Mo, I'm Mo!</p><p>Hello, hello, hello!</p><p>I'm Mo, I'm Mo, I'm Mo!<br></p></div>

            </div>
            </div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_pb_p05.JvcfNRk5"><canvas id="page-canvas-cb864418f894422css_ae_l1_pb_p05" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="pb_06" data-page-number="06" data-type="content" data-book="pupil" data-page-id="cb864418f894422css_ae_l1_pb_p06">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 50px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="4f541d63378cbb3f311350c4fc635409" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_04.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_c5b1cfadc38e4c40f40d71995ca39c24">

        
    	 <p>Stand up<br></p><p>Sit down</p><p>Stand up</p><p>Say “Hello!”</p><p>Sit down</p><p>Say “Hello!”</p><p>Stand up</p><p>Sit down</p><p>Say “Hello!”<br></p></div>

            </div>
            </div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_pb_p06.JvcfNRk5"><canvas id="page-canvas-cb864418f894422css_ae_l1_pb_p06" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="pb_07" data-page-number="07" data-type="content" data-book="pupil" data-page-id="cb867718f897722css_ae_l1_pb_p07">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 55px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="215154869d636710f05813f698070973" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_06.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_3a7818d8bb15d4d6fc7be3f011692415">

        
    	 <p>Clap your hands,<br></p><p>Clap your hands,</p><p>Let's go, let's go.</p><p>And say “Hello”!</p><p><br></p><p>Hello, hello,</p><p>Hello, hello,</p><p>Hello, hello,</p><p>Hello!</p><p><br></p><p>Stand up,</p><p>Sit down,</p><p>Let's go, let's go.</p><p>And say “Hello”!</p><p><br></p><p>Hello, hello, ...</p><p><br></p><p>Clap your hands,</p><p>Clap your hands,</p><p>Let's go, let's go.</p><p>And say “Hello”!</p><p><br></p><p>Hello, hello, ...</p><p><br></p><p>Stand up,</p><p>Sit down,</p><p>Let's go, let's go.</p><p>And say “Hello”!</p><p><br></p><p>Hello, hello, ...</p></div>

            </div>
            

    
                        <div id="17f60c863fbf8ddc4bb1f6fba0b9ba3b" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_07.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_bf6d1c5c7d054a73ce0f39eaa338a560">

        
    	 <p>Clap your hands,<br></p><p>Clap your hands,</p><p>Let's go, let's go.</p><p>And say “Hello”!</p><p><br></p><p>Hello, hello,</p><p>Hello, hello,</p><p>Hello, hello,</p><p>Hello!</p><p><br></p><p>Stand up,</p><p>Sit down,</p><p>Let's go, let's go.</p><p>And say “Hello”!</p><p><br></p><p>Hello, hello, ...</p><p><br></p><p>Clap your hands,</p><p>Clap your hands,</p><p>Let's go, let's go.</p><p>And say “Hello”!</p><p><br></p><p>Hello, hello, ...</p><p><br></p><p>Stand up,</p><p>Sit down,</p><p>Let's go, let's go.</p><p>And say “Hello”!</p><p><br></p><p>Hello, hello, ...</p></div>

            </div>
            </div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_pb_p07.JvcfNRk5"><canvas id="page-canvas-cb867718f897722css_ae_l1_pb_p07" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="ab_04" data-page-number="04" data-type="content" data-book="activity" data-page-id="cb864418f894422css_ae_l1_ab_p04">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
                
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_ab_p04.JvcfNRk5"><canvas id="page-canvas-cb864418f894422css_ae_l1_ab_p04" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="ab_05" data-page-number="05" data-type="content" data-book="activity" data-page-id="cb864418f894422css_ae_l1_ab_p05">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
                
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_ab_p05.JvcfNRk5"><canvas id="page-canvas-cb864418f894422css_ae_l1_ab_p05" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="ab_06" data-page-number="06" data-type="content" data-book="activity" data-page-id="cb864418f894422css_ae_l1_ab_p06">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
        

<div class="resources-tab left" style="top : 55px;">
	
            <div class="sprite icon-toggle"></div>
            

    
                        <div id="43bafb5738c2fd8d3dde75766b9450fc" class="sprite audio-resource" accesskey="" title="Audio" data-audio-src="resources/class_audio/cd1/super_safari_l1_cd1_05.a2fWF5NXu">
                <div class="transcript hidden  " id="transcript_f7aca730bd4039eea8af98a4423b694e">

        
    	 <p>Say “Hello!“<br></p></div>

            </div>
            </div>        
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_ab_p06.JvcfNRk5"><canvas id="page-canvas-cb864418f894422css_ae_l1_ab_p06" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        

 <noscript data-page-title="ab_07" data-page-number="07" data-type="content" data-book="activity" data-page-id="cb867718f897722css_ae_l1_ab_p07">
    
    <span class="sprite nav-button" id="nav-prev"></span>
    <div class="content-image-wrap" data-original-width="890">
                
        <div class="image-wrap">
            <div class="draggable-image-wrap">
                <img class="content-image" src="resources/paginas/ss_ae_l1_ab_p07.JvcfNRk5"><canvas id="page-canvas-cb867718f897722css_ae_l1_ab_p07" class="page-canvas" width="890" height="801"></canvas></div>
        </div>
    </div>
    <span class="sprite nav-button" id="nav-next"></span>


    
	</noscript>
            
        
 
        
    <div id="notepad-container" touch-action="none">
        <canvas id="notepad-canvas-1" class="notepad-canvas"></canvas><canvas id="notepad-canvas-2" class="notepad-canvas"></canvas><canvas id="notepad-canvas-3" class="notepad-canvas"></canvas><span class="sprite close-popup"></span>
        <div id="notepad-breadcrumb">
            <span class="current-page">1</span> of 3
        </div>
        <span class="sprite nav-button active" id="notepad-nav-prev"></span>
        <span class="sprite nav-button active" id="notepad-nav-next"></span>
    </div>
    <div id="draw-toolbar" class="dragable expanded active">
        <div class="sprite toggle"></div>
        <div class="draw-toolbar-content">
            <div class="sprite drag"></div>
            <div id="draw-tools-wrap">
                <div class="sprite draw-tool pointer" data-tool="pointer" title=""></div>
                <div class="sprite draw-tool pencil" data-tool="pencil" title="Pen">
                    <div class="submenu pencil">
                        <div class="submenu-item pencil-stroke small" data-size="6"></div>
                        <div class="submenu-item pencil-stroke default" data-size="12"></div>
                        <div class="submenu-item pencil-stroke big" data-size="40"></div>
                    </div>
                </div>
                <div class="sprite draw-tool marker" data-tool="marker" title="Highlighter">
                    <div class="submenu marker">
                        <div class="submenu-item marker-stroke small" data-size="6"></div>
                        <div class="submenu-item marker-stroke default" data-size="12"></div>
                        <div class="submenu-item marker-stroke big" data-size="40"></div>
                    </div>
                </div>
                <div class="sprite draw-tool rubber" data-tool="rubber" title="Eraser">
                    <div class="submenu rubber">
                        <div class="sprite submenu-item clear-all"></div>
                        <div class="sprite submenu-item clear-line"></div>
                    </div>
                </div>
                <div class="sprite draw-tool palette" data-tool="palette" title="Colour">
                    <div class="submenu palette">
                        <div class="submenu-item color" data-color="#fff" style="background-color:#fff"></div>
                        <div class="submenu-item color" data-color="#000" style="background-color:#000"></div>
                        <div class="submenu-item color" data-color="#ec6f34" style="background-color:#ec6f34"></div>
                        <div class="submenu-item color" data-color="#EC1E24" style="background-color:#EC1E24"></div>
                        <div class="submenu-item color" data-color="#FCF204" style="background-color:#FCF204"></div>
                        <div class="submenu-item color" data-color="#3CB64C" style="background-color:#3CB64C"></div>
                        <div class="submenu-item color" data-color="#04AEEC" style="background-color:#04AEEC"></div>
                        <div class="submenu-item color" data-color="#242264" style="background-color:#242264"></div>
                        <div class="submenu-item color" data-color="#951D87" style="background-color:#951D87"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="item-container">
        <span class="sprite close-item-container"></span>
        <div id="item-container-placeholder"></div>
    </div>

    <div id="activity-overlay" class="hidden">
        <div id="activity-placeholder"></div>
        <span class="sprite close-activity"></span>
    </div>

    <div id="audio-resource-player" class="hidden">
        <div class="header">
            <span class="sprite drag"></span>
            <span class="sprite close"></span>
        </div>
        <div class="body">
            <div id="audio-player-wrap"></div>
            <div id="transcript-buttons" class="audio">
                <button id="toggle-transcript">Show transcript</button>
                <button id="transcript-highlight" class="sprite hidden"></button>
            </div>
            <div id="transcript-wrap" class="hidden"></div>
        </div>
    </div>

    <div id="answer-key-widget" class="hidden">
        <div class="header">
            <span class="sprite key"></span>
            <span class="sprite close"></span>
            <span class="sprite toggle"></span>
        </div>
        <div class="body">
        </div>
        <div class="footer">
            <button id="show-all-keys" class="button button-small active">Show all</button>
            <button id="next-key" class="button button-small active">Next Item</button>
            <button id="prev-key" class="button button-small active">Last Item</button>
        </div>
    </div>

    <input type="file" class="hidden" id="attachment-upload"></div>
<div id="dialog-overlay" class="hidden">
    <div id="dialog-placeholder"></div>
</div>



<noscript data-type="dialog" id="delete-note-dialog-template">
    <div id="delete-note-dialog">
        Are you sure that you want to delete this note? <br><div class="buttons-wrap">
            <button id="delete-note-button-accept" class="button">Yes</button>
            <button id="delete-note-button-cancel" class="button">No</button>
        </div>
    </div>
</noscript>

<noscript data-type="dialog" id="delete-attachment-dialog-template">
    <div id="delete-attachment-dialog">
        Are you sure that you want to delete this attachment? <br><div class="buttons-wrap">
            <button id="delete-attachment-button-accept" class="button">Yes</button>
            <button id="delete-attachment-button-cancel" class="button">No</button>
        </div>
    </div>
</noscript>

<noscript data-type="dialog" id="attachment-file-size-exceded-dialog-template">
    <div id="delete-note-dialog">
        Max file size exceded (1.5Mb)<br><div class="buttons-wrap">
            <button id="generic-button-accept" class="button">Ok</button>
        </div>
    </div>
</noscript>

<noscript data-type="dialog" id="attachment-file-size-exceded-dialog-template">
    <div id="delete-note-dialog">
        Unexpected error saving your file. Filoe too large?<br><div class="buttons-wrap">
            <button id="generic-button-accept" class="button">Ok</button>
        </div>
    </div>
</noscript>

<noscript data-type="dialog" id="attachment-database-error-dialog-template">
    <div id="delete-note-dialog">
        Error accessing local database. Please try again<br><div id="custom-html-placeholder"></div>
        <div class="buttons-wrap">
            <button id="generic-button-accept" class="button">Ok</button>
        </div>
    </div>
</noscript>

<noscript data-type="dialog" id="attachment-not-supported-dialog-template">
    <div id="delete-note-dialog">
        Attachment functionality is not supported on this device.<br><div id="custom-html-placeholder"></div>
        <div class="buttons-wrap">
            <button id="generic-button-accept" class="button">Ok</button>
        </div>
    </div>
</noscript>

<noscript id="audio-player-template">
    <div class="media presentationPlus ">
        <div class="media-body">
                        <div class="netex-audio-player" data-audio-swf-url="./netexAudio.swf" data-audio-player-id="audio_">
                <div class="netex-audio-container">
                    <audio preload="none"><source src="" type="audio/ogg"></source></audio></div>
                <div class="netex-audio-controls-container">
                    <div class="netex-audio-controls">
                        <div class="netex-audio-controls-item netex-audio-button-play-pause-restart netex-audio-button-play"></div>
                        <div class="netex-audio-time-container">
                            <span class="netex-audio-time-current">00:00</span>
                            <div class="netex-audio-progress-bar-container">
                                <div class="netex-audio-progress-bar">
                                    <div class="netex-audio-progress-bar-inner"></div>
                                </div>
                            </div>
                            <span class="netex-audio-time-duration">00:00</span>
                        </div>
                        <div class="netex-audio-volume-control">
                                <div class="netex-audio-volume-slider netex-audio-volume-draggable">
                                    <div class="netex-audio-volume-slider-foreground"></div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</noscript>



<noscript data-type="activity" data-id="activity_e9e2322df4a8dbc2d40654e26fe901dd">
<div>





          <div class="container" style="height:590px; width:740px; position:relative; margin:0 auto;">
            <div id="test_48415b9d-594a-411c-8372-1c48a8649c50" class="testLC_KidsBox testLCComponent" style="height:590px;">
                <div id="4cf81ff3c561894534fa4d7a1491edf7_YBeSos" class="itemTestLC_KidsBox itemTestLCComponent" style="height: 590px; width: 740px; top: 0px; left: 0px;" data-activity-type="T15-Pelmanism" data-type="T15-Pelmanism">
                    <div class="marcoKidsBox " style="height: 547px;"></div>
                    <div class="copyrightKidsBox "></div>
                    <div class="tapaActivity" style="height: 415px;"></div>
                    <div class="enviarComponent"><p>Save</p></div>
                    <div class="disableActivity" style="height: 415px;"></div>
                    <div class="solutionComponent"><p>


See answers                    </p></div>

                    <div class="reiniciaComponent desactive"></div>
                    <div class="comprobarComponent" intentos="2"><p>
Check                    </p></div>
                      <div class="nuevoIntentoComponent"><p>
Try again                    </p></div>

                        <div class="enunciado_KidsBox enunciadoComponent" style="height:48px; width:524.85px; top:20px; left:30px; position:absolute;">
                                                           <p>
<b>Find the pairs.</b>                                    </p>
                        </div>

                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:100px; left:100px; position:absolute;" id="XiwcLS" correcto="BiyuBR" urlmp3="">
                            <div class="carta_v2 divHighlight" id="XiwcLS_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="XiwcLS_img" src="resources/activities/ss_l1_u0_ex1_01.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/ss_l1_u0_ex1_01.JvcfNRk5"></div>



                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:100px; left:247px; position:absolute;" id="wgjJ1E" correcto="sJ3gdF" urlmp3="">
                            <div class="carta_v2 divHighlight" id="wgjJ1E_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="wgjJ1E_img" src="resources/activities/ss_l1_u0_ex1_02.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/ss_l1_u0_ex1_02.JvcfNRk5"></div>



                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:100px; left:394px; position:absolute;" id="xSUH7G" correcto="QTJdQ6" urlmp3="">
                            <div class="carta_v2 divHighlight" id="xSUH7G_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="xSUH7G_img" src="resources/activities/ss_l1_u0_ex1_03.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/ss_l1_u0_ex1_03.JvcfNRk5"></div>



                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:100px; left:540px; position:absolute;" id="OsDSS7" correcto="t7WJPM" urlmp3="">
                            <div class="carta_v2 divHighlight" id="OsDSS7_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="OsDSS7_img" src="resources/activities/ss_l1_u0_ex1_04.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/ss_l1_u0_ex1_04.JvcfNRk5"></div>



                        <div class="feedback_KidsBox feedbackComponent act_positivo" style="display:none;position:absolute;">
                                    <p>Well done!</p>
                                    <div class="closeButton"></div>
                        </div>
                        <div class="feedback_KidsBox feedbackComponent act_medio" style="display:none;position:absolute;">
                                    <p>Try again!</p>
                                    <div class="closeButton"></div>
                        </div>
                        <div class="feedback_KidsBox feedbackComponent act_negativo" style="display:none; position:absolute;">
                                    <p>Check the correct answers.</p>
                                    <div class="closeButton"></div>
                        </div>

                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:295px; left:100px; position:absolute;" id="BiyuBR" correcto="XiwcLS" urlmp3="resources/activities/ss_l1_u0_mike.a2fWF5NXu">
                            <div class="carta_v2 divHighlight" id="BiyuBR_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="BiyuBR_img" src="resources/activities/audio.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/audio.JvcfNRk5"></div>



                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:295px; left:247px; position:absolute;" id="sJ3gdF" correcto="wgjJ1E" urlmp3="resources/activities/ss_l1_u0_gina.a2fWF5NXu">
                            <div class="carta_v2 divHighlight" id="sJ3gdF_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="sJ3gdF_img" src="resources/activities/audio.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/audio.JvcfNRk5"></div>



                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:295px; left:394px; position:absolute;" id="QTJdQ6" correcto="xSUH7G" urlmp3="resources/activities/ss_l1_u0_leo.a2fWF5NXu">
                            <div class="carta_v2 divHighlight" id="QTJdQ6_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="QTJdQ6_img" src="resources/activities/audio.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/audio.JvcfNRk5"></div>



                        <div class="carta_v2 cartaDivComponent turnedUp" style="height:150px; width:101px; top:295px; left:540px; position:absolute;" id="t7WJPM" correcto="OsDSS7" urlmp3="resources/activities/ss_l1_u0_polly.a2fWF5NXu">
                            <div class="carta_v2 divHighlight" id="t7WJPM_div" style="height:150px; width:101px; top:5px; left:5px; opacity:0.2; z-index:50; position:absolute;"></div>
                            <img class="carta_v2 cartaImgComponent" style="height:150px; width:101px; top:5px; left:5px;" id="t7WJPM_img" src="resources/activities/audio.JvcfNRk5" urlreverso="resources/activities/reverso.JvcfNRk5" urlimgaudio="resources/activities/audio.JvcfNRk5" urlrecurso="resources/activities/audio.JvcfNRk5"></div>




                </div>
            </div>
        </div>


</div>


<script>if( typeof funcionalityActivity == 'function'){funcionalityActivity()};if( typeof isSimon == 'function'){ Simon.Main.init();}</script></noscript>

<noscript data-type="activity" data-id="activity_774f8b4e536c8b8ed8796966ae2aa441">
<div>

<div id="simon" class="columns_2" data-allow-repeated-items="no" data-secuence-size="4" data-max-lifes=" 3">
    
        <div id="simon-header">
        <div class="rubric">
        
    <p><strong>Listen and choose in order.</strong><br></p></div>


        <button id="play-btn" class="header-item sprite"></button>
        <div id="score" class="header-item">
            <!--<span id="score-text" class="sprite"></span>-->  
            <span id="score-number"></span>  
        </div>
        <ul id="lifes-counter" class="header-item"><li class="life-icon sprite" data-position="0">

                </li>
                            <li class="life-icon sprite" data-position="1">

                </li>
                            <li class="life-icon sprite" data-position="2">

                </li>
                    </ul><!--<button id="restart-btn" class="sprite"></button>--></div>
  
    <div id="simon-content">
        <div>
            
<div id="card-cad62e17caebd64d1a9517162708e945" class="card" data-audio-src="resources/ss_l1_uhello_mike.a2fWF5NXu"> 
    <img src="resources/ss_l1_u0_ex2_01.JvcfNRk5" class="card-image" width="100%" height="100%"><div class="card-overlay"></div>
</div>

<div id="card-8c50f4a12f4c1973f384bf1f5904bf66" class="card" data-audio-src="resources/ss_l1_uhello_gina.a2fWF5NXu"> 
    <img src="resources/ss_l1_u0_ex2_02.JvcfNRk5" class="card-image" width="100%" height="100%"><div class="card-overlay"></div>
</div>

<div id="card-213cb51069fa61bec2cfacea16af88c9" class="card" data-audio-src="resources/ss_l1_uhello_leo.a2fWF5NXu"> 
    <img src="resources/ss_l1_u0_ex2_03.JvcfNRk5" class="card-image" width="100%" height="100%"><div class="card-overlay"></div>
</div>

<div id="card-2b894435384ef5a86c179875e65ea308" class="card" data-audio-src="resources/ss_l1_uhello_polly.a2fWF5NXu"> 
    <img src="resources/ss_l1_u0_ex2_04.JvcfNRk5" class="card-image" width="100%" height="100%"><div class="card-overlay"></div>
</div>
        </div>
    </div>
    <div class="copyright">
        <h3></h3>
    </div>
    <div id="feedback-ok" class="feedback hidden ">
    </div>
    <div id="feedback-ko" class="feedback hidden ">
    </div>
    <div id="feedback-done" class="feedback hidden ">
    </div>
    <div id="feedback-game-over" class="feedback hidden ">
    </div>
    <audio id="audio-player"></audio><audio id="audioSprite" preload="auto"><source src="commoncontents/audio/audios.a2fWF5NXu" type="audio/ogg"></source></audio></div>

</div>


<script>if( typeof funcionalityActivity == 'function'){funcionalityActivity()};if( typeof isSimon == 'function'){ Simon.Main.init();}</script></noscript>




                  <script src="commoncontents/ntxtmp/js/libs/jquery-2.1.1.min.js"></script><script src="trackingConfig.js"></script><script src="commoncontents/kidsBox/js/libs/mediaelement/mediaelement-and-player.js"></script><script src="commoncontents/kidsBox/js/netex/htmlActivity_min.js"></script><script src="trackingConfig.js"></script><script src="commoncontents/js/lib/underscore-min.js"></script><script src="commoncontents/js/lib/backbone-min.js"></script><script src="commoncontents/js/lib/pointerEvents.js"></script><script src="commoncontents/js/simon.min.js"></script><script src="trackingConfig.js"></script><script src="commoncontents/ntxtmp/js/libs/pointerEvents.js"></script><script src="commoncontents/ntxtmp/js/netex/ntxtmp_min.js"></script><script src="commoncontents/ntxtmp/js/netex/model/dbModel.js"></script><style>
                .Wirisformula:not([width]) {
                    vertical-align: middle !important;
                    margin-bottom: -8px !important;
                }
            </style><style>
                .mathquill_image {
                    vertical-align: middle !important;
                    margin-top: -3px !important;
                }
            </style></body></html>
