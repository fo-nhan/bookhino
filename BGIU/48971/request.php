<?php

//error 400 : "Câu lệnh truy vấn có vấn đề, hãy kiểm tra lại câu lệnh và các tham số truyền vào"
//error 200 : "Truy vấn thành công"
//error 401 : "Database không có dữ liệu"
//error 402 : Không có câu lệnh nào được thực hiện
//error 403 : Lỗi này chắc chắn đến từ câu lệnh SQL bị sai

function REQUEST_GET($sql, $type, array $params)
{
    require './config.php';
    if ($stmt = $db_con->prepare($sql)) {
        $stmt = $db_con->prepare($sql);
        $stmt->bind_param($type, ...$params);
        if (!$stmt->execute()) {
            //Bắn về lỗi nếu truy vấn có vấn đề
            $error = $db_con->errno . ' ' . $db_con->error;
            $stmt->close();

            return ["status" => 400, "mess" => "Đã có lỗi sảy ra trong quá trình xử lý", "resul" => $error];
        } else {
            $result = array();
            $res = $stmt->get_result();
            while ($row = $res->fetch_assoc()) {
                $result[] = $row;
            }
            $stmt->close();
            if (!empty($result)) {
                return ["status" => 200, "mess" => "Thành công", "resul" => $result];
            } else {
                //Bắn ra lỗi 401 nếu không truy vấn được bất kỳ điều gì
                return ["status" => 401, "mess" => "Không có kết quả nào trả về",  "resul" => []];
            }
        }
    } else {
        $error = $db_con->errno . ' ' . $db_con->error;
        return ["status" => 403, "mess" => "Đã có lỗi sảy ra trong quá trình xử lý",  "resul" => $error];
    }
}

function REQUEST_ALL($sql)
{
    require './config.php';
    if ($stmt = $db_con->prepare($sql)) {
        $stmt = $db_con->prepare($sql);
        if (!$stmt->execute()) {
            //Bắn về lỗi nếu truy vấn có vấn đề
            $error = $db_con->errno . ' ' . $db_con->error;
            $stmt->close();
            return ["status" => 400, "mess" => "Đã có lỗi sảy ra trong quá trình xử lý", "resul" => []];
        } else {
            $result = array();
            $res = $stmt->get_result();
            while ($row = $res->fetch_assoc()) {
                $result[] = $row;
            }
            $stmt->close();
            if (!empty($result)) {
                return ["status" => 200, "mess" => "Thành công", "resul" => $result];
            } else {
                //Bắn ra lỗi 401 nếu không truy vấn được bất kỳ điều gì
                return ["status" => 401, "mess" => "Không có kết quả nào trả về",  "resul" => []];
            }
        }
    } else {
        // $error = $db_con->errno . ' ' . $db_con->error;
        return ["status" => 403, "mess" => "Đã có lỗi sảy ra trong quá trình xử lý",  "resul" => []];
    }
}

function REQUEST_POST($sql, $type, array $params)
{
    require './config.php';
    if ($stmt = $db_con->prepare($sql)) {
        $stmt = $db_con->prepare($sql);
        $stmt->bind_param($type, ...$params);
        if (!$stmt->execute()) {
            //Bắn về lỗi nếu truy vấn có vấn đề
            $error = $db_con->errno . ' ' . $db_con->error;
            $stmt->close();
            return ["status" => 400, "mess" => "Đã có lỗi sảy ra trong quá trình xử lý", "resul" => $error];
        } else {
            // $count = $stmt->num_rows();
            // if ($count > 0) {
            $stmt->close();
            return ["status" => 200, "mess" => "Thành công", "resul" => []];
            // } else {
            //     $stmt->close();
            //     return ["status" => 402, "resul" => $stmt];
            // }
        }
    } else {
        $error = $db_con->errno . ' ' . $db_con->error;
        return ["status" => 403, "mess" => "Đã có lỗi sảy ra trong quá trình xử lý",  "resul" => []];
    }
}
function REQUEST_POST_ALL($sql)
{
    require './config.php';
    if ($stmt = $db_con->prepare($sql)) {
        $stmt = $db_con->prepare($sql);
        if (!$stmt->execute()) {
            //Bắn về lỗi nếu truy vấn có vấn đề
            $stmt->close();
            return ["status" => 400, "mess" => "Đã có lỗi sảy ra trong quá trình xử lý", "resul" => []];
        } else {
            // $count = $stmt->num_rows();
            // if ($count > 0) {
            $stmt->close();
            return ["status" => 200, "mess" => "Thành công", "resul" => []];
            // } else {
            //     $stmt->close();
            //     return ["status" => 402, "resul" => $stmt];
            // }
        }
    } else {
        //=$error = $db_con->errno . ' ' . $db_con->error;
        return ["status" => 403, "mess" => "Đã có lỗi sảy ra trong quá trình xử lý",  "resul" => []];
    }
}
function rand_token($lengthz)
{
    $strz = "";
    $charsz = "abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ0123456789";
    $sizez = strlen($charsz);
    for ($i = 0; $i < $lengthz; $i++) {
        $strz .= $charsz[rand(0, $sizez - 1)];
    }
    return $strz;
}
?>