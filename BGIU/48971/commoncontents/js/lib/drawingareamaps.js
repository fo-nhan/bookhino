var nTimeOut=0;

function drawAreaImageMap(area,offsetX,offsetY){
    var coords = $(area).attr("coords").split(",");
    var newLeft = Number(coords[0])+offsetX;
    var newTop = Number(coords[1])+offsetY;
    var newWidth =  Number(coords[2]) - Number(coords[0]) - 2;
    var newHeight = Number(coords[3]) - Number(coords[1]) - 2 ;

    var areaEdit = $("#areaEditOnlineSelect_"+$(area).attr("id"));
    if(areaEdit != undefined && newWidth>0 && newHeight>0){
       $("#areaEditOnlineSelect_"+area.id).css({left:newLeft+"px",top:newTop+"px",width:newWidth+"px",height:newHeight+"px"});
    }

}

function drawAreasImageMap(img){
    if( $(img).attr("usemap") != undefined ){
        var nameMap =$(img).attr("usemap").split("#").join("");
        var pX = Number($(img).offset().left);
        var pY = Number($(img).offset().top);
        var mapResized = $("map[name='"+nameMap+"']")[0];
        var areas = $("area",mapResized);
        for(var i = 0; i<areas.length;i++){
           drawAreaImageMap(areas[i],pX,pY);
        }
    }

}

function drawAllAreasImageMap(){
   $.each(  $('img[usemap]'), function(index,obj){ drawAreasImageMap(obj);});
}

function drawAllAreasImageMapWithRetard(miliseconds){
   if(!miliseconds) miliseconds = 100;
   if(nTimeOut != 0) clearTimeout(nTimeOut); //La idea es eliminar las actualizaciones continuadas...
   nTimeOut = setTimeout(function(){ drawAllAreasImageMap(); }, miliseconds);
}
