<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<?php

session_start();
require './config.php';
require './request.php';
$login_failed_message = "";

try {
  $pdo = new PDO('sqlite:users.db');
} catch (PDOException $e) {
  exit("Error: " . $e->getMessage());
}

if (isset($_POST['submit'])) {
  $email = $_POST['email'];
  $password = $_POST['password'];
  $sql = "SELECT * FROM account WHERE account = ? AND password = ? ";
  $request = REQUEST_GET($sql, "ss", [$email, $password]);

  if ($request["status"] == 200) {
    $sql3 = "UPDATE `access` SET `status` = 'remove' WHERE `access`.`id` = ?";
    $request3 = REQUEST_POST($sql3, "s", [$request["resul"][0]['id']]);
    $token = rand_token(50);
    $_SESSION['user_id'] = $request["resul"][0]['id'];
    $_SESSION['book1'] = $request["resul"][0]['book1'];
    $_SESSION['book2'] = $request["resul"][0]['book2'];
    $_SESSION['book3'] = $request["resul"][0]['book3'];
    $_SESSION['book4'] = $request["resul"][0]['book4'];
    $_SESSION['book5'] = $request["resul"][0]['book5'];
    $_SESSION['book6'] = $request["resul"][0]['book6'];
    $_SESSION['token'] = $token;
    $startTime = date("Y-m-d H:i:s");
    $cenvertedTime = date('Y-m-d H:i:s',strtotime('+5 minutes',strtotime($startTime)));
    $sql2 = "INSERT INTO `access` (`stt`, `id`, `token`, `timeEnd`, `status`) VALUES (NULL, ?, ?, ?, 'normal')";
    $request2 = REQUEST_POST($sql2, "sss", [$request["resul"][0]['id'],$token, $cenvertedTime]);
    //;
    echo '<script> window.location="index.php"; </script> ';
  } else {
    //echo '<p class="error">Email and Password combination is wrong!</p>';
    $login_failed_message = "Wrong Email or Password";
    //echo $login_failed_message;
  }
}


?>

<!DOCTYPE HTML>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta charset="utf-8">
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="page_type" content="np-template-header-footer-from-plugin">
  <title>Login to hino0905 Books online</title>
  <link rel="stylesheet" href="nicepage.css" media="screen">
  <link rel="stylesheet" href="Login.css" media="screen">
  <script class="u-script" type="text/javascript" src="jquery.js" defer=""></script>
  <script class="u-script" type="text/javascript" src="nicepage.js" defer=""></script>
  <meta name="generator" content="Nicepage 4.7.1, nicepage.com">
  <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i">

  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "name": "",
      "logo": "images/cropped-English-logo.png"
    }
  </script>
  <meta name="theme-color" content="#478ac9">
  <meta property="og:title" content="Login">
  <meta property="og:type" content="website">
</head>

<body class="u-body u-xl-mode">
  <header class="u-clearfix u-header" id="sec-6bff">
    <div class="u-clearfix u-sheet u-sheet-1">
      <a href="https://book.hino0905.icu" class="u-image u-logo u-image-1" data-image-width="412" data-image-height="412">
        <img src="images/cropped-English-logo.png" class="u-logo-image u-logo-image-1">
      </a>
      <nav class="u-menu u-menu-dropdown u-offcanvas u-menu-1">
        <div class="menu-collapse" style="font-size: 1rem; letter-spacing: 0px;">
          <a class="u-button-style u-custom-left-right-menu-spacing u-custom-padding-bottom u-custom-top-bottom-menu-spacing u-nav-link u-text-active-palette-1-base u-text-hover-palette-2-base" href="#">
            <svg class="u-svg-link" viewBox="0 0 24 24">
              <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#menu-hamburger"></use>
            </svg>
            <svg class="u-svg-content" version="1.1" id="menu-hamburger" viewBox="0 0 16 16" x="0px" y="0px" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
              <g>
                <rect y="1" width="16" height="2"></rect>
                <rect y="7" width="16" height="2"></rect>
                <rect y="13" width="16" height="2"></rect>
              </g>
            </svg>
          </a>
        </div>
        <div class="u-custom-menu u-nav-container">
          <ul class="u-nav u-unstyled u-nav-1">
            <li class="u-nav-item"><a class="u-button-style u-nav-link u-text-active-palette-1-base u-text-hover-palette-2-base" href="index.php" style="padding: 10px 20px;">Home</a>
            </li>
            <li class="u-nav-item"><a class="u-button-style u-nav-link u-text-active-palette-1-base u-text-hover-palette-2-base" href="login.php" style="padding: 10px 20px;">Login</a>
            </li>
            <li class="u-nav-item"><a class="u-button-style u-nav-link u-text-active-palette-1-base u-text-hover-palette-2-base" href="Contact.php" style="padding: 10px 20px;">Contact</a>
            </li>
          </ul>
        </div>
        <div class="u-custom-menu u-nav-container-collapse">
          <div class="u-black u-container-style u-inner-container-layout u-opacity u-opacity-95 u-sidenav">
            <div class="u-inner-container-layout u-sidenav-overflow">
              <div class="u-menu-close"></div>
              <ul class="u-align-center u-nav u-popupmenu-items u-unstyled u-nav-2">
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="index.php">Home</a>
                </li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="LOGIN.php">Login</a>
                </li>
                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="Contact.php">Contact</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="u-black u-menu-overlay u-opacity u-opacity-70"></div>
        </div>
      </nav>
      <h5 class="u-text u-text-1">www.book.hino0905.icu&nbsp; &nbsp;&nbsp;</h5>
    </div>




    <head>
      <title> Book online</title>
      <script src="js/jquery.min.js"></script>
      <!-- Custom Theme files -->
      <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
      <!-- for-mobile-apps -->
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="keywords" content="Classy Login form Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
      <!-- //for-mobile-apps -->
      <!--Google Fonts-->
      <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
    </head>
    <!--header start here-->
    <div class="header">
      <div class="header-main">
        <h1>Login to read books</h1>
        <div class="header-bottom">
          <div class="header-right w3agile">

            <div class="header-left-bottom agileinfo">

              <form action="#" method="post">
                <input type="text" value="email" name="email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'email';}" />
                <input type="password" value="password" name="password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'password';}" />

                <div class="remember">
                  <span class="checkbox1">
                    <label class="checkbox"><input type="checkbox" name="" checked=""><i> </i>Remember me</label>
                  </span>
                  <div class="forgot">
                    <h6><a href="Contact.php">Forgot Password?</a></h6>
                  </div>
                  <div class="clear"> </div>
                </div>

                <input type="submit" value="submit" name="submit">
                <h6 style="color: red;" class="u-align-center u-text u-text-custom-color-6 u-text-default u-text-2"> <?php echo $login_failed_message; ?> </h6>
              </form>

            </div>
          </div>

        </div>
      </div>

    </div>




    <!--header end here-->
    <div class="copyright">
      <p>© 2022 Hino0905.icu | Design by <a href="https:hino0905.icu/" target="_blank"> Books lover </a></p>
    </div>
    <!--footer end here-->
</body>

</html>