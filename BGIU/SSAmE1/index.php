
<?php

    
session_start();

require './config.php';
require './request.php';

if (isset($_SESSION['token']) && isset($_SESSION['user_id']) && $_SESSION['token'] && $_SESSION['user_id']) {
    $sql = "SELECT * FROM access WHERE `access`.`id` = ? AND `access`.`token` = ? AND `access`.`status` = 'normal' ";
    $request = REQUEST_GET($sql, "ss", [$_SESSION['user_id'], $_SESSION['token']]);
    if ($request['status'] !== 200) {
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s', strtotime('+5 minutes', strtotime($startTime)));
        $sql4 = "UPDATE `access` SET `timeEnd` = ? WHERE `access`.`id` = ? AND `access`.`token` = ? AND `access`.`status` = 'normal'";
        $request4 = REQUEST_POST($sql4, "sss", [$cenvertedTime, $_SESSION['user_id'], $_SESSION['token']]);
        session_unset();
        session_destroy();
        echo '<script> alert("You are logged in somewhere else!"); </script> ';
        echo '<script> window.location="login.php"; </script> ';
    }
} else {
    session_unset();
    session_destroy();
}
    
    if(!isset($_SESSION['book1'])){
        
        header('Location: ../LOGIN.php');
        exit;
    } 
    else if ($_SESSION['book1'] == 0)
    {
        header('Location: ../index.php');
        exit;
    }
    
?>


<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="apple-mobile-web-app-capable" content="yes"><meta name="apple-mobile-web-app-status-bar-style" content="black"><meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"><meta name="author" content="Netex"><meta name="dcterms.created" content="05-02-2016 12:36:09"><!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=Edge"><![endif]--><title>Presentation Plus</title><link rel="stylesheet" href="commoncontents/ntxtmp/css/netex/ntxtmp_min.css"><meta name="learningmaker-version" content="4.2.3.10"></head><body data-course-id="super_safari_l1_ae">
            <div id="page-wrap" class="landscape">
	<div id="main-menu-wrap">
    <ul class="main-menu enabled" id="main-menu"><li class="main-menu-item home">  
            <a class="sprite" title="Please select a unit to activate these areas"></a>
        </li>
        <li class="main-menu-item pupil-book active">
            <span class="sprite" title="Student's Book"></span>
            <ul class="navigator pupil-book-navigator"></ul></li>
        <li class="main-menu-item activity-book active">
           <span class="sprite" title="Workbook"></span>
            <ul class="navigator activity-book-navigator"></ul></li>
        <li class="main-menu-item media">
            <span class="sprite" title="Please select a unit to activate these areas"></span>
            
        </li>
        <li class="main-menu-item resource-popup">
            <span class="sprite" title="Please select a unit to activate these areas"></span>
        </li>
        <li class="main-menu-item link-popup">
            <span class="sprite" title="Please select a unit to activate these areas"></span>
        </li>
        <li class="main-menu-item bookmark">
            <span class="sprite" title="Please select a unit to activate these areas"></span>
            
        </li>
        <li class="main-menu-item note">
            <span class="sprite" title="Please select a unit to activate these areas"></span>
            
        </li>
        <li class="main-menu-item mask">
            <span class="sprite" title="Please select a unit to activate these areas"></span>
        </li>
        <li class="main-menu-item notepad ">
            <span class="sprite" title="Please select a unit to activate these areas"></span>
        </li>
    </ul><div id="title-bar">
              
    </div>
    
</div>

    <div id="pageView" class="content" touch-action="none" data-trans-out-forward="fadeOut" data-trans-out-rewind="fadeOut" data-trans-in="fadeIn"></div>

    

<div class="page-content">
    <div class="content-image-wrap">
        <img class="content-image" src="resources/ss_l1_ae.JvcfNRk5"><div id="links-wrap">
            <div id="tab-content-wrap">
                <div id="tab-content-pupil-book" class="tab-content">
                    <div class="titleMenu"> Student's Book</div>
                    <div class="sprite navMenu prevBlock hidden"></div>
                    <div class="sprite navMenu nextBlock hidden"></div>
                                           <ul class="block-0 "><li class="link">

        <span class="bullet">
            0        </span>
            <a href="48971/index.php" class="unit-title">
                Hello            </a>
    </li>

    <li class="link">

        <span class="bullet">
            1        </span>
            <a href="48972/index.php" class="unit-title">
                My class            </a>
    </li>

    <li class="link">

        <span class="bullet">
            2        </span>
            <a href="48973/index.php" class="unit-title">
                My colors            </a>
    </li>

    <li class="link">

        <span class="bullet">
            3        </span>
            <a href="48974/index.php" class="unit-title">
                My family            </a>
    </li>

    <li class="link">

        <span class="bullet">
            4        </span>
            <a href="48975/index.php" class="unit-title">
                My toys            </a>
    </li>

    <li class="link">

        <span class="bullet">
            5        </span>
            <a href="48976/index.php" class="unit-title">
                My numbers            </a>
    </li>

    <li class="link">

        <span class="bullet">
            6        </span>
            <a href="48977/index.php" class="unit-title">
                My pets            </a>
    </li>

    <li class="link">

        <span class="bullet">
            7        </span>
            <a href="48978/index.php" class="unit-title">
                My food            </a>
    </li>

    <li class="link">

        <span class="bullet">
            8        </span>
            <a href="48979/index.php" class="unit-title">
                My clothes            </a>
    </li>

    <li class="link">

        <span class="bullet">
            9        </span>
            <a href="48980/index.php" class="unit-title">
                My park            </a>
    </li>

    <li class="link">

        <span class="bullet">
                    </span>
            <a href="48981/index.php" class="unit-title">
                Phonics            </a>
    </li>

    <li class="link">

        <span class="bullet">
                    </span>
            <a href="48982/index.php" class="unit-title">
                Review            </a>
    </li>
                        </ul></div>
                <div id="tab-content-activity-book" class="tab-content hidden" data-count-links="10">
                      <div class="titleMenu"> Workbook</div>
                    <div class="sprite navMenu prevBlock hidden"></div>
                    <div class="sprite navMenu nextBlock hidden"></div>

                                            <ul class="block-0"><li class="link">

        <span class="bullet">
            0        </span>
            <a href="48971/index.html#book/activity" class="unit-title">
                Hello            </a>
    </li>

    <li class="link">

        <span class="bullet">
            1        </span>
            <a href="48972/index.html#book/activity" class="unit-title">
                My class            </a>
    </li>

    <li class="link">

        <span class="bullet">
            2        </span>
            <a href="48973/index.html#book/activity" class="unit-title">
                My colors            </a>
    </li>

    <li class="link">

        <span class="bullet">
            3        </span>
            <a href="48974/index.html#book/activity" class="unit-title">
                My family            </a>
    </li>

    <li class="link">

        <span class="bullet">
            4        </span>
            <a href="48975/index.html#book/activity" class="unit-title">
                My toys            </a>
    </li>

    <li class="link">

        <span class="bullet">
            5        </span>
            <a href="48976/index.html#book/activity" class="unit-title">
                My numbers            </a>
    </li>

    <li class="link">

        <span class="bullet">
            6        </span>
            <a href="48977/index.html#book/activity" class="unit-title">
                My pets            </a>
    </li>

    <li class="link">

        <span class="bullet">
            7        </span>
            <a href="48978/index.html#book/activity" class="unit-title">
                My food            </a>
    </li>

    <li class="link">

        <span class="bullet">
            8        </span>
            <a href="48979/index.html#book/activity" class="unit-title">
                My clothes            </a>
    </li>

    <li class="link">

        <span class="bullet">
            9        </span>
            <a href="48980/index.html#book/activity" class="unit-title">
                My park            </a>
    </li>
                    </ul></div>
            </div>
        </div>
    </div>
</div>
</div>
        
        <div id="browser-compatibility-dialog" class="hidden">
            <div id="dialog-content">
                <p>Presentation Plus works best with:</p>
                <ul><li>Firefox</li>
                    <li>Chrome</li>
                    <li>Safari</li>
                </ul><button id="button-accept" class="button">Ok</button>
            </div>
        </div>

        
                    <script src="commoncontents/ntxtmp/js/netex/ntxtmp_min.js"></script><style>
                .Wirisformula:not([width]) {
                    vertical-align: middle !important;
                    margin-bottom: -8px !important;
                }
            </style><style>
                .mathquill_image {
                    vertical-align: middle !important;
                    margin-top: -3px !important;
                }
            </style></body></html>
