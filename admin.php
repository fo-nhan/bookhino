<?php

session_start();

require './config.php';
require './request.php';

if (isset($_SESSION['token']) && isset($_SESSION['user_id']) && $_SESSION['token'] && $_SESSION['user_id']) {
    $sql = "SELECT * FROM access WHERE `access`.`id` = ? AND `access`.`token` = ? AND `access`.`status` = 'normal' ";
    $request = REQUEST_GET($sql, "ss", [$_SESSION['user_id'], $_SESSION['token']]);
    if ($request['status'] !== 200) {
        $startTime = date("Y-m-d H:i:s");
        $cenvertedTime = date('Y-m-d H:i:s', strtotime('+5 minutes', strtotime($startTime)));
        $sql4 = "UPDATE `access` SET `timeEnd` = ? WHERE `access`.`id` = ? AND `access`.`token` = ? AND `access`.`status` = 'normal'";
        $request4 = REQUEST_POST($sql4, "sss", [$cenvertedTime, $_SESSION['user_id'], $_SESSION['token']]);
        session_unset();
        session_destroy();
        echo '<script> alert("You are logged in somewhere else!"); </script> ';
    }
} else {
    session_unset();
    session_destroy();
}

include "./admin-action.php"


?>

<!DOCTYPE html>
<html style="font-size: 16px;">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Hino0905 online book, Grammar in use">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Admin</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="nicepage.css" media="screen">
    <link rel="stylesheet" href="./Home.css" media="screen">
    <link rel="stylesheet" href="./admin.css" media="screen">
    <script class="u-script" type="text/javascript" src="jquery.js" defer=""></script>
    <script class="u-script" type="text/javascript" src="nicepage.js" defer=""></script>
    <meta name="generator" content="Nicepage 4.7.1, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">




    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "name": "",
            "logo": "images/cropped-English-logo.png"
        }
    </script>
    <meta name="theme-color" content="#478ac9">
    <meta property="og:title" content="Home">
    <meta property="og:type" content="website">
</head>

<body data-home-page="index.php" data-home-page-title="Home" class="u-body u-xl-mode">
    <script>
        window.addEventListener("click", () => {
            const token = localStorage.getItem('token');
            if (token) {
                fetch('checkpoint.php', {
                        method: 'GET', // or 'PUT'
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                    .then((response) => response.json())
                    .then((data) => {
                        if (data && data.checkpoint) {
                            alert("You are logged in somewhere else!");
                            <?php echo 'window.location="login.php"; '; ?>
                        }
                    })
            }
        })
    </script>
    <header class="u-clearfix u-header" id="sec-6bff">
        <div style="display:flex ; align-items:center ;justify-content: space-between ;" class="u-clearfix u-sheet u-sheet-1">
            <a style="display: flex ;align-items: center; gap: 10px; font-weight: 500;" href="index.php" class="u-image u-logo u-image-1" data-image-width="512" data-image-height="512">
                <img src="images/cropped-English-logo.png" class="u-logo-image u-logo-image-1">hino0905.icu
            </a>
            <nav class="u-menu u-menu-dropdown u-offcanvas ">
                <div class="menu-collapse" style="font-size: 1rem; letter-spacing: 0px;">
                    <a class="u-button-style u-custom-left-right-menu-spacing u-custom-padding-bottom u-custom-top-bottom-menu-spacing u-nav-link u-text-active-palette-1-base u-text-hover-palette-2-base" href="#">
                        <svg class="u-svg-link" viewBox="0 0 24 24">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#menu-hamburger"></use>
                        </svg>
                        <svg class="u-svg-content" version="1.1" id="menu-hamburger" viewBox="0 0 16 16" x="0px" y="0px" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg">
                            <g>
                                <rect y="1" width="16" height="2"></rect>
                                <rect y="7" width="16" height="2"></rect>
                                <rect y="13" width="16" height="2"></rect>
                            </g>
                        </svg>
                    </a>
                </div>
                <div class="u-custom-menu u-nav-container">
                    <ul class="u-nav u-unstyled u-nav-1">
                        <li class="u-nav-item"><a class="u-button-style u-nav-link u-text-active-palette-1-base u-text-hover-palette-2-base" href="index.php" style="padding: 10px 20px;">Home</a>
                        </li>



                        <li class="u-nav-item"><a class="u-button-style u-nav-link u-text-active-palette-1-base u-text-hover-palette-2-base" href="Contact.php" style="padding: 10px 20px;">Contact</a>
                        </li>
                        <li class="u-nav-item">
                            <?php if (!isset($_SESSION['user_id'])) { ?>
                                <a class="u-button-style u-nav-link u-text-active-palette-1-base u-text-hover-palette-2-base" href="login.php" style="padding: 10px 20px;">Login</a>
                            <?php } ?>
                        </li>

                        <?php if (isset($_SESSION['user_id'])) { ?>
                            <li class="u-nav-item profile">
                                <div style="border-radius: 100%;height: 50px;width: 50px; display: flex;align-items: center;justify-content: center;cursor: pointer">
                                    <img src="images/avatar.png" style="width:100%;height:100%;border-radius:100%;" />
                                </div>
                                <div class="logout">
                                    <?php if (isset($_SESSION['role']) && $_SESSION['role'] == "admin") { ?>
                                        <a style="width: 100%;height: 40px;" href="admin.php">
                                            <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 640 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M610.5 341.3c2.6-14.1 2.6-28.5 0-42.6l25.8-14.9c3-1.7 4.3-5.2 3.3-8.5-6.7-21.6-18.2-41.2-33.2-57.4-2.3-2.5-6-3.1-9-1.4l-25.8 14.9c-10.9-9.3-23.4-16.5-36.9-21.3v-29.8c0-3.4-2.4-6.4-5.7-7.1-22.3-5-45-4.8-66.2 0-3.3.7-5.7 3.7-5.7 7.1v29.8c-13.5 4.8-26 12-36.9 21.3l-25.8-14.9c-2.9-1.7-6.7-1.1-9 1.4-15 16.2-26.5 35.8-33.2 57.4-1 3.3.4 6.8 3.3 8.5l25.8 14.9c-2.6 14.1-2.6 28.5 0 42.6l-25.8 14.9c-3 1.7-4.3 5.2-3.3 8.5 6.7 21.6 18.2 41.1 33.2 57.4 2.3 2.5 6 3.1 9 1.4l25.8-14.9c10.9 9.3 23.4 16.5 36.9 21.3v29.8c0 3.4 2.4 6.4 5.7 7.1 22.3 5 45 4.8 66.2 0 3.3-.7 5.7-3.7 5.7-7.1v-29.8c13.5-4.8 26-12 36.9-21.3l25.8 14.9c2.9 1.7 6.7 1.1 9-1.4 15-16.2 26.5-35.8 33.2-57.4 1-3.3-.4-6.8-3.3-8.5l-25.8-14.9zM496 368.5c-26.8 0-48.5-21.8-48.5-48.5s21.8-48.5 48.5-48.5 48.5 21.8 48.5 48.5-21.7 48.5-48.5 48.5zM96 224c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm224 32c1.9 0 3.7-.5 5.6-.6 8.3-21.7 20.5-42.1 36.3-59.2 7.4-8 17.9-12.6 28.9-12.6 6.9 0 13.7 1.8 19.6 5.3l7.9 4.6c.8-.5 1.6-.9 2.4-1.4 7-14.6 11.2-30.8 11.2-48 0-61.9-50.1-112-112-112S208 82.1 208 144c0 61.9 50.1 112 112 112zm105.2 194.5c-2.3-1.2-4.6-2.6-6.8-3.9-8.2 4.8-15.3 9.8-27.5 9.8-10.9 0-21.4-4.6-28.9-12.6-18.3-19.8-32.3-43.9-40.2-69.6-10.7-34.5 24.9-49.7 25.8-50.3-.1-2.6-.1-5.2 0-7.8l-7.9-4.6c-3.8-2.2-7-5-9.8-8.1-3.3.2-6.5.6-9.8.6-24.6 0-47.6-6-68.5-16h-8.3C179.6 288 128 339.6 128 403.2V432c0 26.5 21.5 48 48 48h255.4c-3.7-6-6.2-12.8-6.2-20.3v-9.2zM173.1 274.6C161.5 263.1 145.6 256 128 256H64c-35.3 0-64 28.7-64 64v32c0 17.7 14.3 32 32 32h65.9c6.3-47.4 34.9-87.3 75.2-109.4z"></path>
                                            </svg>
                                            Admin
                                        </a>
                                    <?php } ?>
                                    <a style="width: 100%;height: 40px;" href="info.php">
                                        <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 448 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"></path>
                                        </svg>
                                        Profile
                                    </a>
                                    <a style="width: 100%;height: 40px;" href="logout.php">
                                        <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 24 24" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                            <g id="Logout">
                                                <g>
                                                    <path d="M20.968,18.448a2.577,2.577,0,0,1-2.73,2.5c-2.153.012-4.306,0-6.459,0a.5.5,0,0,1,0-1c2.2,0,4.4.032,6.6,0,1.107-.016,1.589-.848,1.589-1.838V5.647A1.546,1.546,0,0,0,19,4.175a3.023,3.023,0,0,0-1.061-.095H11.779a.5.5,0,0,1,0-1c2.224,0,4.465-.085,6.687,0a2.567,2.567,0,0,1,2.5,2.67Z"></path>
                                                    <path d="M3.176,11.663a.455.455,0,0,0-.138.311c0,.015,0,.028-.006.043s0,.027.006.041a.457.457,0,0,0,.138.312l3.669,3.669a.5.5,0,0,0,.707-.707L4.737,12.516H15.479a.5.5,0,0,0,0-1H4.737L7.552,8.7a.5.5,0,0,0-.707-.707Z"></path>
                                                </g>
                                            </g>
                                        </svg>
                                        Log out
                                    </a>
                                </div>

                            </li>
                        <?php  } ?>
                    </ul>
                </div>
                <div class="u-custom-menu u-nav-container-collapse">
                    <div class="u-black u-container-style u-inner-container-layout u-opacity u-opacity-95 u-sidenav">
                        <div class="u-inner-container-layout u-sidenav-overflow">
                            <div class="u-menu-close"></div>
                            <ul class="u-align-center u-nav u-popupmenu-items u-unstyled u-nav-2">
                                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="index.php">Home</a>
                                </li>

                                <?php if (isset($_SESSION['user_id'])) { ?>
                                    <li class="u-nav-item"><a class="u-button-style u-nav-link" href="info.php">Profile</a>
                                    </li>
                                <?php  } ?>
                                <?php if (isset($_SESSION['role']) && $_SESSION['role'] == "admin") { ?>
                                    <li class="u-nav-item"><a class="u-button-style u-nav-link" href="admin.php">Admin</a>
                                    </li>
                                <?php  } ?>

                                <li class="u-nav-item"><a class="u-button-style u-nav-link" href="Contact.php">Contact</a>
                                </li>
                                <?php if (isset($_SESSION['user_id'])) { ?>
                                    <li class="u-nav-item"><a class="u-button-style u-nav-link" href="logout.php">Logout</a>
                                    </li>
                                <?php  } else { ?>
                                    <li class="u-nav-item"><a class="u-button-style u-nav-link" href="login.php">Login</a>
                                    </li>
                                <?php  } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="u-black u-menu-overlay u-opacity u-opacity-70"></div>
                </div>
            </nav>
        </div>
    </header>
    <section class="admin-container" id="carousel_3edc">
        <div class="admin-box-title">
            Danh sách người dùng <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenterks">Thêm mới</button>
        </div>
        <br />

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col" style="width: 200px">Fullname</th>
                    <th scope="col" style="width: 200px">Account</th>
                    <th scope="col" style="width: 100px">Password</th>
                    <th scope="col" style="width: 100px">Role</th>
                    <th scope="col" style="width: 200px">Status</th>
                    <th scope="col">Note</th>
                    <th scope="col" style="width: 150px">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($accounts)) {
                    foreach ($accounts as $account) {

                        echo '<tr>
                                    <th scope="row">' . $account["id"] . '</th>
                                    <td>' . $account["fullname"] . '</td>
                                    <td>' . $account["account"] . '</td>
                                    <td>' . $account["password"] . '</td>
                                    <td>' . $account["role"] . '</td>
                                    <td>' . ($account["status"] == "normal" ? "Đang hoạt động" : "Không hoạt động") . '</td>
                                    <td>' . $account["note"] . '</td>
                                    <td><button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModalCenter-' . $account["id"] . '">Update</button></td>
                                </tr>
                                <div class="modal fade" id="exampleModalCenter-' . $account["id"] . '"" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="#" method="post">
                                                <div class="modal-body">
                                                    <input value="' . $account["id"] . '" type="text" class="form-control" name="id" style="display: none;" placeholder="ID" aria-label="ID" aria-describedby="basic-addon1" required>
                                                    <input value="' . $account["fullname"] . '" type="text" class="form-control" name="fullname" placeholder="Fullname" aria-label="Fullname" aria-describedby="basic-addon1" required>
                                                    <br/>
                                                    <input value="' . $account["account"] . '" type="text" class="form-control" name="account" placeholder="Account" aria-label="Account" aria-describedby="basic-addon1" required>
                                                    <br/>
                                                    <input value="' . $account["password"] . '" type="text" class="form-control" name="password" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" required>
                                                    <br/>
                                                    <select value="' . $account["role"] . '" class="custom-select custom-select-xl mb-3" name="role" required >
                                                        <option value="admin" selected>Admin</option>
                                                        <option value="member">Member</option>
                                                    </select>
                                                    <br/>
                                                    <select value="' . $account["status"] . '" class="custom-select custom-select-xl mb-3" name="status" required>
                                                        <option value="normal" selected>Đang hoạt động</option>
                                                        <option value="remove">Tạm dừng</option>
                                                    </select>
                                                    <br/>
                                                    <textarea placeholder="Ghi chú" class="form-control" name="note" aria-label="With textarea">' . $account["note"] . '</textarea>
                                                    <br/>
                                                    <label>Chọn sách</label>
                                                    <br/>
                                                    ';

                        foreach ($account as $key => $item) {
                            if (strpos($key, 'book') !== false) {  // Kiểm tra nếu key chứa chuỗi "book"
                                echo '<div class="form-check form-check-inline">
                                                            ' . ($item == "1" ? '<input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="' . $key . '" value="1" checked>' : '<input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="' . $key . '" value="1">') . '
                                                                    
                                                                    <label class="form-check-label" for="inlineCheckbox1">' . $key . '</label>
                                                                </div>';
                            }
                        }

                        echo '</div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button value="Update" name="submit" type="submit" class="btn btn-primary">Update</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                
                                ';
                    }
                }

                ?>
            </tbody>
        </table>

        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenterks" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">

                <div class="modal-content" style="width: 100%;max-width: 500px;">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Thêm mới</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="#" method="post">
                        <div class="modal-body">
                            <input type="text" class="form-control" name="fullname" placeholder="Fullname" aria-label="Fullname" aria-describedby="basic-addon1" required>
                            <br />
                            <input type="text" class="form-control" name="account" placeholder="Account" aria-label="Account" aria-describedby="basic-addon1" required>
                            <br />
                            <input type="text" class="form-control" name="password" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" required>
                            <br />
                            <select class="custom-select custom-select-xl mb-3" name="role" required>
                                <option value="admin" selected>Admin</option>
                                <option value="member">Member</option>
                            </select>
                            <br />
                            <select class="custom-select custom-select-xl mb-3" name="status" required>
                                <option value="normal" selected>Đang hoạt động</option>
                                <option value="remove">Tạm dừng</option>
                            </select>
                            <br />
                            <textarea placeholder="Ghi chú" class="form-control" name="note" aria-label="With textarea"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button value="Save" name="submit" type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>


    <footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" id="sec-384f">
        <div class="u-clearfix u-sheet u-sheet-1">
            <p class="u-small-text u-text u-text-variant u-text-1"> Contact us for more details. Email: hino09051986@gmail.com; Viber/Whatsapp: +84 774703685.</p>
        </div>
    </footer>
    <section class="u-backlink u-clearfix u-grey-80">
        <a class="u-link" href="https://book.hino0905.icu" target="_blank">
            <span>interactive books</span>
        </a>
        <p class="u-text">
            <span>created by</span>
        </p>
        <a class="u-link" href="https://book.hino0905.icu" target="_blank">
            <span>Books Hino</span>
        </a>.
    </section>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>