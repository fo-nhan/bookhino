<?php

$accounts = [];

if (isset($_SESSION['user_id']) && $_SESSION['user_id']) {
    $sql2 = "SELECT * FROM account WHERE id = ? ";
    $request2 = REQUEST_GET($sql2, "s", [$_SESSION['user_id']]);

    if ($request2["status"] == 200 && $request2["resul"][0] && $request2["resul"][0]["role"] == "admin") {
        echo '<script>
      console.log("Access")
      </script> ';
    } else {
        echo '<script>
        window.location="index.php"; 
        </script> ';
    }
} else {
    echo '<script>
        window.location="index.php"; 
        </script> ';
}

$sql = "SELECT * FROM account ";
$request = REQUEST_ALL($sql);
if ($request["status"] == 200) {
    $accounts = $request["resul"];
}

if (isset($_POST['submit'])  && $_POST['submit'] == "Save" && isset($_SESSION['user_id'])) {
    $account = $_POST['account'];
    $password = $_POST['password'];
    $fullname = $_POST['fullname'];
    $role = $_POST['role'];
    $status = $_POST['status'];
    $note = $_POST['note'];
    $sql = "INSERT INTO `account` (`id`, `fullname`, `account`, `password`, `role`, `status`, `note`) VALUES (NULL, ?,?,?,?,?, ?)";
    $request = REQUEST_POST($sql, "sssss", [$fullname, $account, $password, $role, $status, $note]);

    if ($request["status"] == 200) {
        echo '<script>
      window.location="admin.php"; 
      </script> ';
    } else {
        echo '<script> alert("Error Created!"); </script> ';
    }
}

if (isset($_POST['submit'])  && $_POST['submit'] == "Update" && isset($_SESSION['user_id'])) {

    $arr = [];
    $arr[] = $_POST['account'];
    $arr[] = $_POST['password'];
    $arr[] = $_POST['fullname'];
    $arr[] = $_POST['role'];
    $arr[] = $_POST['status'];
    $arr[] = $_POST['note'];
    $stringKey = "sssssss";

    $sql = "UPDATE `account` SET `account` = ?, `password` = ?, `fullname` = ?, `role` = ?, `status` = ?, `note` = ?";

    $sql2 = "SELECT * FROM account WHERE id = ? ";
    $request2 = REQUEST_GET($sql2, "s", [$_POST['id']]);

    if ($request2["status"] == 200) {
        foreach ($request2["resul"][0] as $key => $item) {
            if (strpos($key, 'book') !== false) {
                $sql = $sql . ', `' . $key . '` = ?';
                $arr[] = isset($_POST[$key]) ? 1 : 0;
                $stringKey = $stringKey . "s";
            }
        }
    }


    $sql = $sql . " WHERE `id` = ?";
    $arr[] = $_POST['id'];

    $request = REQUEST_POST($sql, $stringKey, $arr);

    // echo '<script> console.log("request ",' . json_encode($request2) . ') </script> ';
    // echo '<script> console.log("arr ",' . json_encode($arr) . ') </script> ';
    // echo '<script> console.log("key ",' . json_encode($stringKey) . ') </script> ';
    // echo '<script> console.log("sql ",' . json_encode($sql) . ') </script> ';

    if ($request["status"] == 200) {
        echo '<script>
          window.location="admin.php"; 
          </script> ';
    } else {
        echo '<script> alert("Error Created!"); </script> ';
    }
}
