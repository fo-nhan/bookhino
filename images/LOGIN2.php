<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

<?php

session_start();
$login_failed_message = "";

try
{
    $pdo = new PDO ('sqlite:users.db');
}
catch (PDOException $e) 
{
        exit("Error: " . $e->getMessage());
}

if (isset($_POST['submit'])) {
        $email = $_POST['username'];
        $password = $_POST['password'];
        $query_select = $pdo->prepare("SELECT * FROM users WHERE email=:email");
        $query_select->bindParam("email", $email, PDO::PARAM_STR);
        $query_select->execute();
        $result = $query_select->fetch(PDO::FETCH_ASSOC);
        
        //$login_failed_message = '<p class="error">Email and Password combination is wrong!</p>';
        
        
        if (!$result) {
            $login_failed_message = "Wrong Email or Password";
            //echo $login_failed_message;
        } 
        else {
        
            if ($password == $result['password']) {
                $_SESSION['user_id'] = $result['email'];
                $_SESSION['book1'] = $result['book1'];
                $_SESSION['book2'] = $result['book2'];
                $_SESSION['book3'] = $result['book3'];
                $_SESSION['book4'] = $result['book4'];
                $_SESSION['book5'] = $result['book5'];
                $_SESSION['book6'] = $result['book6'];
                
              
                echo'<script> window.location="index.php"; </script> ';

                

            } else {
                //echo '<p class="error">Email and Password combination is wrong!</p>';
                $login_failed_message = "Wrong Email or Password";
                //echo $login_failed_message;
            }
        }
}


?>

<!DOCTYPE HTML>
<html>
<head>
<title>English0905 Book online</title>
<script src="js/jquery.min.js"></script>
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!-- for-mobile-apps -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta name="keywords" content="Classy Login form Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- //for-mobile-apps -->
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
</head>
<body>
<!--header start here-->
<div class="header">
		<div class="header-main">
		       <h1>Login to read book</h1>
			<div class="header-bottom">
				<div class="header-right w3agile">
					
					<div class="header-left-bottom agileinfo">
				<form action="" method="POST">
				<!--  <form action="" method="POST" class="u-clearfix u-form-custom-backend u-form-spacing-20 u-form-vertical u-inner-form" source="custom" name="form-3" style="padding: 0px;"> 
				-->
				
              <!--  <div class="u-form-group u-form-password">
                  <label for="username-5b0a" class="u-form-control-hidden u-label"></label>
                  <input type="email" placeholder="Enter your Email" id="username-5b0a" name="username" class="u-border-2 u-border-white u-input u-input-rectangle u-radius-45 u-white u-input-1" required="" autofocus="autofocus">
                </div> -->
                <div class="u-form-group u-form-password">
				      <label for="username-5b0a" class="u-form-control-hidden u-label"></label>
                  <input type="email" placeholder="Enter your Email 0905" id="username-5b0a" name="username" class="u-border-2 u-border-white u-input u-input-rectangle u-radius-45 u-white u-input-1" required="" autofocus="autofocus">
				  
                  <label for="password-5b0a" class="u-form-control-hidden u-label"></label>
                  <input type="password" placeholder="Enter your Password 0905" id="password-5b0a" name="password" class="u-border-2 u-border-white u-input u-input-rectangle u-radius-45 u-white u-input-2" required="">
                </div>
                <!--
                <div class="u-form-checkbox u-form-group">
                  <input type="checkbox" id="checkbox-5b0a" name="remember" value="On">
                  <label for="checkbox-5b0a" class="u-label">Remember me</label>
                </div>
                -->
                
				<div class="u-align-left u-form-group u-form-submit">
                  <!-- <a href="#" class="u-border-none u-btn u-btn-round u-btn-submit u-button-style u-radius-13 u-btn-1">Login</a> -->
                  <input type="submit" value="submit" name="submit" class="u-form-control-hidden">
                </div>
								
                <input type="hidden" value="" name="recaptchaResponse">
				 <h6 class="u-align-center u-text u-text-custom-color-6 u-text-default u-text-2" > <?php echo $login_failed_message; ?> </h6>
              </form>					
					
					<!-- <form action="#" method="post">
						<!-- <input type="text"  value="username" name="username" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'username';}"/> 
						
					<!-- <input type="password"  value="password" name="password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'password';}"/> 
					 
									
						<div class="remember">
			             <span class="checkbox1">
							   <label class="checkbox"><input type="checkbox" name="" checked=""><i> </i>Remember me</label>
						 </span>
						 <div class="forgot">
						 	<h6><a href="#">Forgot Password?</a></h6>
						 </div>
						<div class="clear"> </div>
					  </div>
					   
						<input type="submit" value="Login">
					</form>	
						-->										
				</div>
				</div>
			  
			</div>
		</div>
		
		
		
		
		<div class="u-custom-menu u-nav-container">
                    <ul class="u-nav u-spacing-2 u-unstyled u-nav-1"><li class="u-nav-item"><a class="u-border-2 u-border-active-grey-90 u-border-hover-grey-50 u-button-style u-nav-link u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="index.php" style="padding: 10px 20px;">HOME</a>
</li><li class="u-nav-item"><a class="u-border-2 u-border-active-grey-90 u-border-hover-grey-50 u-button-style u-nav-link u-text-active-grey-90 u-text-grey-90 u-text-hover-grey-90" href="LOGIN.php" style="padding: 10px 20px;">LOGIN</a>
</li></ul>
                  </div>
                  <div class="u-custom-menu u-nav-container-collapse">
                    <div class="u-black u-container-style u-inner-container-layout u-opacity u-opacity-95 u-sidenav">
                      <div class="u-inner-container-layout u-sidenav-overflow">
                        <div class="u-menu-close"></div>
                        <ul class="u-align-center u-nav u-popupmenu-items u-unstyled u-nav-2"><li class="u-nav-item"><a class="u-button-style u-nav-link" href="index.php" style="padding: 10px 20px;">HOME</a>
</li><li class="u-nav-item"><a class="u-button-style u-nav-link" href="LOGIN.php" style="padding: 10px 20px;">LOGIN</a>
</li></ul>
                      </div>
                    </div>
                    <div class="u-black u-menu-overlay u-opacity u-opacity-70"></div>
                  </div>
                </nav>
                <a href="index.php" data-page-id="952711351" class="u-align-center-lg u-align-center-md u-align-center-sm u-align-center-xs u-image u-logo u-image-2" data-image-width="100" data-image-height="100" title="HOME">
                  <img src="images/logo1.png" class="u-logo-image u-logo-image-1">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div></header>
    <section class="u-align-center u-clearfix u-section-1" id="carousel_286f">
      <div class="u-clearfix u-sheet u-sheet-1">
        <img class="u-align-center u-image u-image-contain lazyload u-image-1" data-image-width="117" data-image-height="100" data-src="images/93.png">
        <div class="u-align-center u-container-style u-expanded-width-sm u-expanded-width-xs u-grey-10 u-group u-opacity u-opacity-40 u-shape-rectangle u-group-1">
         
        </div>
      </div>
    </section>
</div>
<!--header end here-->
<div class="copyright">
	<p>© 2022 English0905.com | Design by  <a href="http://w3layouts.com/" target="_blank">  Book lover </a></p>
</div>
<!--footer end here-->
</body>
</html>