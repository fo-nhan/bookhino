<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

session_start();
require './config.php';
require './request.php';

if (isset($_SESSION['token']) && isset($_SESSION['user_id']) && $_SESSION['token'] && $_SESSION['user_id']) {
    $sql = "SELECT * FROM access WHERE `access`.`id` = ? AND `access`.`token` = ? AND `access`.`status` = 'normal' ";
    $request = REQUEST_GET($sql, "ss", [$_SESSION['user_id'], $_SESSION['token']]);
    if ($request['status'] !== 200) {
      $startTime = date("Y-m-d H:i:s");
      $cenvertedTime = date('Y-m-d H:i:s', strtotime('+5 minutes', strtotime($startTime)));
      $sql4 = "UPDATE `access` SET `timeEnd` = ? WHERE `access`.`id` = ? AND `access`.`token` = ? AND `access`.`status` = 'normal'";
      $request4 = REQUEST_POST($sql4, "sss", [$cenvertedTime, $_SESSION['user_id'], $_SESSION['token']]);
      session_unset();
      session_destroy();
        echo json_encode(array("checkpoint" => true));
    }else{
        echo json_encode(array("checkpoint" => false));
    }
  } else {
    echo json_encode(array("checkpoint" => false));
  }

?>