<?php
renderBook('book1', "SAFARI LEVEL 1", 'thumb.jpg', ["Interacive book", "(A1-B1) learner", "Safari 1"], [[
    'title' => 'READ Level 1 NOW',
    'url' => 'BGIU/index.php'
]]);
renderBook('book2', "SAFARI LEVEL 2", 'thumb1.jpg', ["Interacive book", "(B1-B2) learner", "Safari 2"], [[
    'title' => 'READ Level 2 NOW',
    'url' => 'GIU/index.php'
]]);
renderBook('book3', "SAFARI LEVEL 3", 'thumb2.jpg', ["Interacive book", "(C1-C2) learner", "Safari 3"], [[
    'title' => 'READ Level 3 NOW',
    'url' => 'stream/gw0/on/index.php'
]]);
renderBook('book4', "SAFARI LEVEL 4", 'thumb3.jpg', ["Interacive book", "(A1 B1) learner", "Safari 4"], [[
    'title' => 'READ Level 4 NOW',
    'url' => 'EGIU/index.php'
]]);
?>




<?php

if (isset($_SESSION['user_id']) && $_SESSION['user_id']) {
    $sql2 = "SELECT * FROM account WHERE id = ? ";
    $request2 = REQUEST_GET($sql2, "s", [$_SESSION['user_id']]);

    if ($request2["status"] == 200) {
        echo '<script>
      console.log("Access")
      </script> ';
    } else {
        echo '<script>
        window.location="index.php"; 
        </script> ';
    }
} else {
    echo '<script>
        window.location="index.php"; 
        </script> ';
}

?>



<?php
function renderBook($bookId, $name, $image, $arrayContent, $links)
{
    if (isset($_SESSION['user_id'])) {

        $sql2 = 'SELECT * FROM account WHERE `' . $bookId . '` = "1" AND id = ? ';
        $request2 = REQUEST_GET($sql2, "s", [$_SESSION['user_id']]);

        if ($request2["status"] == 200 && count($request2["resul"]) > 0) {

?>
            <div class="u-container-style u-list-item u-repeater-item u-white u-list-item-1 profile-box-content-book">
                <div class="u-container-layout u-similar-container u-container-layout-1">
                    <h4 class="u-align-center u-custom-font u-font-montserrat u-text u-text-palette-4-base u-text-2"><?= $name ?></h4>
                    <img alt="" class="u-image u-image-default u-image-1" src="images/<?= $image ?>" data-image-width="300" data-image-height="414">
                    <ul class="u-align-left u-custom-list u-text u-text-3">
                        <?php
                        foreach ($arrayContent as $content) {
                            echo '<li>
                        <div class="u-list-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" class="u-svg-content" viewBox="0 0 512 512" id="svg-7189">
                                <path d="m433.1 67.1-231.8 231.9c-6.2 6.2-16.4 6.2-22.6 0l-99.8-99.8-78.9 78.8 150.5 150.5c10.5 10.5 24.6 16.3 39.4 16.3 14.8 0 29-5.9 39.4-16.3l282.7-282.5z" fill="currentColor"></path>
                            </svg>
                        </div>
                        <span style="font-weight: 700;">' . $content . '</span>
                    </li>';
                        }

                        ?>
                    </ul>
                    <?php
                    foreach ($links as $link) {
                        echo '<a href="' . $link["url"] . '" class="u-btn u-btn-rectangle u-button-style u-palette-4-base u-btn-1">' . $link["title"] . '</a>';
                    }
                    echo '<a href="Contact.php" class="u-btn u-btn-rectangle u-button-style u-palette-4-base u-btn-1">FREE PDF , AUDIO</a>';
                    ?>

                </div>
            </div>
<?php }
    }
}
?>